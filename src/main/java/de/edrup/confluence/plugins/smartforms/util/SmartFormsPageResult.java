package de.edrup.confluence.plugins.smartforms.util;

import java.util.ArrayList;
import java.util.List;

public class SmartFormsPageResult {
	
	private int totalSize;
	private List<Long> results;

	public SmartFormsPageResult() {
		this.results = new ArrayList<Long>();
		this.totalSize = 0;
	}
	
	public SmartFormsPageResult(List<Long> results, int totalSize) {
		this.results = results;
		this.totalSize = totalSize;
	}
	
	public List<Long> getResults() {
		return results;
	}
	
	public int getTotalSize() {
		return totalSize;
	}
	
	public void setTotalSize(int totalSize) {
		this.totalSize = totalSize;
	}
	
	public int getSize() {
		return results.size();
	}
	
	public void addAll(List<Long> moreResults) {
		this.results.addAll(moreResults);
	}
}
