package de.edrup.confluence.plugins.smartforms.macros;

import java.util.Map;

import javax.inject.Inject;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.renderer.radeox.macros.MacroUtils;
import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.util.velocity.VelocityUtils;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;

import de.edrup.confluence.plugins.smartforms.util.SmartFormsHelper;

public class SmartFormsInstanceMacro implements Macro {
	
	private final SmartFormsHelper smartFormsHelper;
	private final PermissionManager permissionMan;
	
	private static String noOutputFor = "pdf word email";

	
	@Inject
	public SmartFormsInstanceMacro(SmartFormsHelper smartFormsHelper, @ComponentImport PermissionManager permissionMan) {
		this.smartFormsHelper = smartFormsHelper;
		this.permissionMan = permissionMan;
	}

	@Override
	public String execute(Map<String, String> parameters, String bodyContent, ConversionContext conversionContext) throws MacroExecutionException {
		
		if(noOutputFor.contains(conversionContext.getOutputType())) {
			return "";
		}
		
		Long pageId = conversionContext.getEntity().getId();
		Map<String, Object> context = MacroUtils.defaultVelocityContext();
		boolean isWorkflowDefined = smartFormsHelper.isWorkflowDefined(pageId);
		context.put("isWorkflowDefined", isWorkflowDefined);
		if(isWorkflowDefined) {
			context.put("stateName", smartFormsHelper.getCurrentStateName(pageId));
			context.put("stateColor", smartFormsHelper.getCurrentStateColor(pageId));
			context.put("stateTextColor", smartFormsHelper.getCurrentStateTextColor(pageId));
			context.put("stateTooltip", smartFormsHelper.getCurrentStateTooltip(pageId));
			context.put("transitions", smartFormsHelper.getTransitions(pageId));
			context.put("currentAssignedUser", smartFormsHelper.getAssignedUserName(pageId));
			context.put("allowedAssignmentGroup", smartFormsHelper.getAllowedAssignmentGroups(pageId));
			context.put("currentUserIsAgent", smartFormsHelper.isCurrentUserAgent(pageId));
			context.put("currentUserIsInAssignmentGroup", smartFormsHelper.isCurrentUserInAssignmentGroups(pageId));
			context.put("currentUsersGroups", smartFormsHelper.getCurrentUsersGroupsAsString());
		}
		context.put("formWithHtml", smartFormsHelper.getApplicableForm(pageId)
			.replace("$attachmentSection", smartFormsHelper.renderAttachmentSection(pageId))
			.replace("$renderedSection", smartFormsHelper.renderRenderedSection(pageId)));
		context.put("formAllowed", smartFormsHelper.lastChangeMadeBySpaceOrConfAdmin(smartFormsHelper.getApplicableFormPageId(pageId)));
		context.put("currentFormData", smartFormsHelper.getCurrentFormData(pageId));
		context.put("currentFormVersion", smartFormsHelper.getFormVersion(pageId));
		context.put("editAllowed", permissionMan.hasPermission(AuthenticatedUserThreadLocal.get(), Permission.EDIT, conversionContext.getEntity()) &&
			smartFormsHelper.formEditForCurrentUserAllowed(pageId));
		context.put("cloningAllowed", smartFormsHelper.isCloningAllowed(pageId));
		return VelocityUtils.getRenderedTemplate("/templates/smart-forms-form-instance.vm", context);
	}

	
	@Override
	public BodyType getBodyType() {
		return BodyType.NONE;
	}

	
	@Override
	public OutputType getOutputType() {
		return OutputType.BLOCK;
	}
}
