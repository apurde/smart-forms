package de.edrup.confluence.plugins.smartforms.macros;

import java.util.Map;
import java.util.Optional;

import javax.inject.Inject;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.storage.macro.MacroId;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.renderer.radeox.macros.MacroUtils;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.user.ConfluenceUser;
import com.atlassian.confluence.util.velocity.VelocityUtils;
import com.atlassian.confluence.xhtml.api.MacroDefinition;
import com.atlassian.json.jsonorg.JSONObject;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.message.I18nResolver;

import de.edrup.confluence.plugins.smartforms.util.SmartFormsHelper;

public class SmartFormsOverviewMacro implements Macro {
	
	private final SmartFormsHelper smartFormsHelper;
	private final I18nResolver i18n;

	
	@Inject
	public SmartFormsOverviewMacro(SmartFormsHelper smartFormsHelper, @ComponentImport I18nResolver i18n) {
		this.smartFormsHelper = smartFormsHelper;
		this.i18n = i18n;
	}

	
	@Override
	public String execute(Map<String, String> parameters, String bodyContent, ConversionContext conversionContext) throws MacroExecutionException {
		
		String query = parameters.containsKey("queryString") ? parameters.get("queryString") : "";
		ConfluenceUser currentUser = AuthenticatedUserThreadLocal.get();
		query = query.replace("$currentUserKey", currentUser.getKey().getStringValue()).
			replace("$currentUserFullName", smartFormsHelper.getCleanedFullName(currentUser.getFullName())).
			replace("$currentUserEmail", currentUser.getEmail()).
			replace("$department", smartFormsHelper.getAttributeFromADForUser(currentUser.getName(), "department")).
			replace("$currentUserName",  currentUser.getName());
		
		MacroDefinition macroDefinition = (MacroDefinition)conversionContext.getProperty("macroDefinition");
		Optional<MacroId> option = macroDefinition.getMacroIdentifier();
		String macroId = "";
		try {
			macroId = option.get().getId();
		}
		catch(Exception e) {
			return i18n.getText("de.edrup.confluence.plugins.smart-forms.overview.noMacroId");
		}
	
		Map<String, Object> context = MacroUtils.defaultVelocityContext();
		context.put("query", query);
		context.put("columns", parameters.containsKey("columns") ? parameters.get("columns") : "");
		context.put("pageId", conversionContext.getEntity().getId());
		context.put("pageSize", parameters.containsKey("pageSize") ? parameters.get("pageSize") : "10");
		context.put("macroId", macroId);
		context.put("showExport", parameters.containsKey("showExport") ? parameters.get("showExport").equals("true") : false);
		Long templatePageId = conversionContext.hasProperty("formPageId") ? (Long) conversionContext.getProperty("formPageId") : conversionContext.getEntity().getId();
		context.put("initialOverviewWithHtml", new JSONObject(smartFormsHelper.getOverview(templatePageId, macroId, "", 0, 0, conversionContext.getEntity().getId())).get("renderedTemplate"));
		
		if(smartFormsHelper.lastChangeMadeBySpaceOrConfAdmin(templatePageId)) {
			return VelocityUtils.getRenderedTemplate("/templates/smart-forms-overview-container.vm", context);
		}
		else {
			return i18n.getText("de.edrup.confluence.plugins.smart-forms.overview.forbidden");
		}
	}

	
	@Override
	public BodyType getBodyType() {
		return BodyType.PLAIN_TEXT;
	}

	
	@Override
	public OutputType getOutputType() {
		return OutputType.BLOCK;
	}
}
