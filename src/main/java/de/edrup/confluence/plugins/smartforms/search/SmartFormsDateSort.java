package de.edrup.confluence.plugins.smartforms.search;

import com.atlassian.confluence.search.v2.SearchSort;
import com.atlassian.confluence.search.v2.sort.FieldSort;

public class SmartFormsDateSort  implements SearchSort {

	private Order order;
	private String key;
	
	public SmartFormsDateSort(String key, Order order) {
		this.order = order;
		this.key = key;
	}
	
	@Override
	public SearchSort.Order getOrder() {
		return order;
	}

	@Override
	public String getKey() {
		return key;
	}
	
	@Override
    public SearchSort expand() {
        return new FieldSort(key, Type.LONG, order);
    }
}
