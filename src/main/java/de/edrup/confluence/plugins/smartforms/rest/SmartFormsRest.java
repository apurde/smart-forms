package de.edrup.confluence.plugins.smartforms.rest;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.CacheControl;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import com.atlassian.json.jsonorg.JSONObject;

import de.edrup.confluence.plugins.smartforms.util.SmartFormsHelper;

@Path("/")
public class SmartFormsRest {
	
	private final SmartFormsHelper smartFormsHelper;
	
	
	@Inject
	public SmartFormsRest(SmartFormsHelper smartFormsHelper) {
		this.smartFormsHelper = smartFormsHelper;
	}
	
	
	@PUT
    @Produces({MediaType.APPLICATION_JSON})
	@Path("/createNewFormPage")
    public Response createNewFormPage(@QueryParam("formPageId") Long formPageId, @QueryParam("workflowPageId") Long workflowPageId, @QueryParam("parentPageId") Long parentPageId, @QueryParam("key") String key) {
		Long pageId = smartFormsHelper.createPage(formPageId, workflowPageId, parentPageId, key);
		return Response.ok("{\"id\":" + pageId.toString() + "}").cacheControl(getNoStoreNoCacheControl()).build();
    }
	
	
	@PUT
	@Produces({MediaType.APPLICATION_JSON})
	@Path("/cloneFormPage")
    public Response cloneFormPage(@QueryParam("clonePageId") Long clonePageId) {
		Long pageId = smartFormsHelper.clonePage(clonePageId, false);
		return Response.ok("{\"id\":" + pageId.toString() + "}").cacheControl(getNoStoreNoCacheControl()).build();
    }
	
	
	@PUT
	@Produces({MediaType.APPLICATION_JSON})
	@Path("/multiCloneFormPage")
    public Response multiCloneFormPage(@QueryParam("clonePageId") Long clonePageId, @QueryParam("count") int count) {
		smartFormsHelper.multiClonePage(clonePageId, count, true);
		return Response.ok("").cacheControl(getNoStoreNoCacheControl()).build();
    }
	
	
	@PUT
    @Produces({MediaType.APPLICATION_JSON})
	@Consumes({MediaType.TEXT_PLAIN})
	@Path("/saveFormData")
    public Response saveFormData(@QueryParam("pageId") Long pageId, @QueryParam("version") Integer version, String data) {
		if(!smartFormsHelper.hasViewPermission(pageId) || !smartFormsHelper.checkFormVersion(pageId, version)) return Response.status(Status.FORBIDDEN).build();
		int versionAfterSet = smartFormsHelper.setFormData(pageId, data);
		return Response.ok(new JSONObject().put("version", versionAfterSet).toString()).build();
    }
	
	
	@PUT
    @Produces({MediaType.APPLICATION_JSON})
	@Consumes({MediaType.TEXT_PLAIN})
	@Path("/setFieldValue")
    public Response setFieldValue(String bodyContent) {
		JSONObject payload = new JSONObject(bodyContent);
		if(!smartFormsHelper.hasViewPermission(payload.getLong("pageId"))) return Response.status(Status.FORBIDDEN).build();
		smartFormsHelper.setFieldValue(payload.getLong("pageId"), payload.getString("fieldName"), payload.getString("value"), payload.getBoolean("overwrite"));
		return Response.ok(smartFormsHelper.getAllFormData(payload.getLong("pageId")).toString()).build();
    }
	
	
	@PUT
    @Produces({MediaType.APPLICATION_JSON})
	@Path("/executeTransition")
    public Response executeTransition(@QueryParam("pageId") Long pageId, @QueryParam("transitionName") String transitionName) {
		if(!smartFormsHelper.hasViewPermission(pageId)) return Response.status(Status.FORBIDDEN).build();
		return Response.ok(smartFormsHelper.executeTransitionTransactional(pageId, transitionName)).cacheControl(getNoStoreNoCacheControl()).build();
    }
	
	
	@PUT
    @Produces({MediaType.APPLICATION_JSON})
	@Path("/reIndex")
    public Response reIndex(@QueryParam("pageId") Long pageId) {
		smartFormsHelper.reIndexPage(pageId);
		return Response.ok("{}").cacheControl(getNoStoreNoCacheControl()).build();
    }
	
	
	@PUT
    @Produces({MediaType.APPLICATION_JSON})
	@Path("/findTransitionAndExecute")
    public Response findAndExecuteTransition(@QueryParam("pageId") Long pageId,  @QueryParam("to") String to) {
		if(!smartFormsHelper.hasViewPermission(pageId)) return Response.status(Status.FORBIDDEN).build();
		return Response.ok(smartFormsHelper.findAndExecuteTransitionTransactional(pageId, to)).cacheControl(getNoStoreNoCacheControl()).build();
    }
	
	
	@GET
	@Produces({MediaType.APPLICATION_JSON})
	@Path("/getUsers")
    public Response getUsers(@QueryParam("query") String query, @QueryParam("groupName") String groupName) {
		return Response.ok(groupName.equals("active-directory") ? smartFormsHelper.getUsersFromAD(query) : smartFormsHelper.getUsers(query, groupName)).cacheControl(getNoStoreNoCacheControl()).build();
	}
	
	
	@PUT
	@Produces({MediaType.APPLICATION_JSON})
	@Path("/assignUser")
    public Response assignUser(@QueryParam("pageId") Long pageId, @QueryParam("userName") String userName) {
		if(!smartFormsHelper.hasViewPermission(pageId)) return Response.status(Status.FORBIDDEN).build();
		return Response.ok(smartFormsHelper.assignUser(pageId, userName) ? "{\"success\":true}" : "{\"success\":false}").cacheControl(getNoStoreNoCacheControl()).build();
	}
	
	
	@GET
	@Produces({MediaType.APPLICATION_JSON})
	@Path("/overview")
    public Response overview(@QueryParam("templatePageId") Long templatePageId, @QueryParam("macroId") String macroId, @QueryParam("query") String query, @QueryParam("start") int start, @QueryParam("limit") int limit,
    	@DefaultValue("0") @QueryParam("embeddingPageId") Long embeddingPageId) {
		return Response.ok(smartFormsHelper.getOverview(templatePageId, macroId, query, start, limit, embeddingPageId)).cacheControl(getNoStoreNoCacheControl()).build();
	}

	
	@GET
	@Produces({MediaType.APPLICATION_JSON})
	@Path("/overviewJSON")
    public Response overviewJSON(@QueryParam("query") String query, @QueryParam("start") int start, @QueryParam("limit") int limit) {
		return Response.ok(smartFormsHelper.getOverviewJSON(query, start, limit).toString()).cacheControl(getNoStoreNoCacheControl()).build();
	}
	
	
	@GET
	@Path("/export")
	@Produces({MediaType.TEXT_PLAIN})
	public Response export(@QueryParam("query") String query, @QueryParam("columns") String columns) {
		return Response.ok(smartFormsHelper.createExcelFile(query, columns)).cacheControl(getNoStoreNoCacheControl()).build();
	}
	
	
	@GET
	@Path("/async")
	@Produces({MediaType.TEXT_HTML})
	public Response export(@QueryParam("pageId") Long pageId, @QueryParam("macroId") String macroId) {
		return Response.ok(smartFormsHelper.getRenderedMacro(pageId, macroId)).cacheControl(getNoStoreNoCacheControl()).build();
	}
	
	
	@GET
	@Produces({MediaType.APPLICATION_JSON})
	@Path("/getAllData")
	public Response getAllData(@QueryParam("pageId") Long pageId) {
		if(!smartFormsHelper.hasViewPermission(pageId)) return Response.status(Status.FORBIDDEN).build();
		return Response.ok(smartFormsHelper.getAllFormData(pageId).toString()).cacheControl(getNoStoreNoCacheControl()).build();
	}
	
	
	@GET
	@Produces({MediaType.TEXT_PLAIN})
	@Path("/getFromAD")
	public Response getFromAD(@QueryParam("queryField") String queryField, @QueryParam("query") String query, @QueryParam("attribute") String attribute) {
		return Response.ok(smartFormsHelper.getAttributeFromAD(queryField, query, attribute)).cacheControl(getNoStoreNoCacheControl()).build();
	}
	
	
	private CacheControl getNoStoreNoCacheControl() {
		CacheControl cc = new CacheControl();
		cc.setNoCache(true);
		cc.setNoStore(true);
		cc.setMustRevalidate(true);
		return cc;
	}
}
