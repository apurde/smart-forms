package de.edrup.confluence.plugins.smartforms.macros;

import java.util.Map;

import javax.inject.Inject;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.renderer.radeox.macros.MacroUtils;
import com.atlassian.confluence.util.velocity.VelocityUtils;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.message.I18nResolver;

import de.edrup.confluence.plugins.smartforms.util.SmartFormsHelper;

public class SmartFormsFormMacro implements Macro {
	
	private final SmartFormsHelper smartFormsHelper; 
	private final I18nResolver i18n;
	
	
	@Inject
	public SmartFormsFormMacro(SmartFormsHelper smartFormsHelper, @ComponentImport I18nResolver i18n) {
		this.smartFormsHelper = smartFormsHelper;
		this.i18n = i18n;
	}

	
	@Override
	public String execute(Map<String, String> parameters, String bodyContent, ConversionContext conversionContext) throws MacroExecutionException {
		if(smartFormsHelper.lastChangeMadeBySpaceOrConfAdmin(conversionContext.getEntity().getId())) {
			Map<String, Object> context = MacroUtils.defaultVelocityContext();
			context.put("formWithHtml", bodyContent);
			return VelocityUtils.getRenderedTemplate("/templates/smart-forms-form.vm", context);
		}
		else {
			return i18n.getText("de.edrup.confluence.plugins.smart-forms.form.forbidden");
		}
	}

	
	@Override
	public BodyType getBodyType() {
		return BodyType.PLAIN_TEXT;
	}

	
	@Override
	public OutputType getOutputType() {
		return OutputType.BLOCK;
	}
}
