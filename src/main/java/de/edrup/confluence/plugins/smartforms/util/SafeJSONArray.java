package de.edrup.confluence.plugins.smartforms.util;

import com.atlassian.json.jsonorg.JSONArray;

public class SafeJSONArray extends JSONArray {
	
	public SafeJSONArray() {
		super();
	}
	
	
	public SafeJSONArray(String fromString) {
		super(fromString);
	}
	
	
	public SafeJSONObject safeGetJSONObject(int index) {
		return new SafeJSONObject(getJSONObject(index).toString());
	}
}
