package de.edrup.confluence.plugins.smartforms.macros;

import java.util.Map;

import javax.inject.Inject;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.renderer.radeox.macros.MacroUtils;
import com.atlassian.confluence.util.velocity.VelocityUtils;

import de.edrup.confluence.plugins.smartforms.util.SmartFormsHelper;

public class SmartFormsCreateMacro implements Macro {
	
	private final SmartFormsHelper smartFormsHelper;
	
	@Inject
	public SmartFormsCreateMacro(SmartFormsHelper smartFormsHelper) {
		this.smartFormsHelper = smartFormsHelper;
	}
	
	
	@Override
	public String execute(Map<String, String> parameters, String bodyContent, ConversionContext conversionContext) throws MacroExecutionException {
		
		Map<String, Object> context = MacroUtils.defaultVelocityContext();
		if(parameters.containsKey("parentPage")) {
			context.put("parentPageId", smartFormsHelper.getPageIdFromPageTitle(parameters.get("parentPage"), conversionContext.getSpaceKey()));
		}
		else {
			context.put("parentPageId", "");
		}
		context.put("formPageId", smartFormsHelper.getPageIdFromPageTitle(parameters.get("formPage"), conversionContext.getSpaceKey()));
		context.put("workflowPageId", smartFormsHelper.getPageIdFromPageTitle(parameters.get("workflowPage"), conversionContext.getSpaceKey()));
		context.put("key",  parameters.get("key").replace("$", conversionContext.getEntity().getTitle()));
		context.put("linkWithHtml", bodyContent);
		
		return VelocityUtils.getRenderedTemplate("/templates/smart-forms-create.vm", context);
	}

	
	@Override
	public BodyType getBodyType() {
		return BodyType.RICH_TEXT;
	}

	
	@Override
	public OutputType getOutputType() {
		return OutputType.BLOCK;
	}
}
