package de.edrup.confluence.plugins.smartforms.search;

public class SmartFormsTextSort2 extends SmartFormsEqualityFieldHandler {
	
	private static final String FIELD_NAME = "smartformstextsort2";
	
	public SmartFormsTextSort2() {
		super(FIELD_NAME);
	}
}
