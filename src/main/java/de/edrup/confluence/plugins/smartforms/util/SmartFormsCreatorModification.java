package de.edrup.confluence.plugins.smartforms.util;

import com.atlassian.confluence.core.Modification;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.user.ConfluenceUser;

public class SmartFormsCreatorModification implements Modification<Page> {
	
	private final ConfluenceUser newCreator;
	private final String versionComment;
	
	public SmartFormsCreatorModification(ConfluenceUser newCreator, String versionComment) {
		this.newCreator = newCreator;
		this.versionComment = versionComment;
	}

	@Override
	public void modify(Page p) {
		p.setCreator(newCreator);
		p.setVersionComment(versionComment);
	}
}