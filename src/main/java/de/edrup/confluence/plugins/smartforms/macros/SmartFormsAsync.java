package de.edrup.confluence.plugins.smartforms.macros;

import java.util.Map;
import java.util.Optional;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.storage.macro.MacroId;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.renderer.radeox.macros.MacroUtils;
import com.atlassian.confluence.util.velocity.VelocityUtils;
import com.atlassian.confluence.xhtml.api.MacroDefinition;


public class SmartFormsAsync implements Macro {
	
	@Override
	public String execute(Map<String, String> parameters, String bodyContent, ConversionContext conversionContext) throws MacroExecutionException {
		
		MacroDefinition macroDefinition = (MacroDefinition) conversionContext.getProperty("macroDefinition");
		Optional<MacroId> macroId = macroDefinition.getMacroIdentifier();
		String macroIdString = "";
		try {
			macroIdString = macroId.get().getId();
		}
		catch(Exception e) {
			macroIdString = "unknown";
		}
		
		Map<String, Object> context = MacroUtils.defaultVelocityContext();
		context.put("macroId", macroIdString);
		
		return VelocityUtils.getRenderedTemplate("/templates/smart-forms-async.vm", context);
	}
	

	@Override
	public BodyType getBodyType() {
		return BodyType.RICH_TEXT;
	}

	
	@Override
	public OutputType getOutputType() {
		return OutputType.BLOCK;
	}
}
