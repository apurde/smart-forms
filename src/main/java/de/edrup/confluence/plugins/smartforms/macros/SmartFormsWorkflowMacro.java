package de.edrup.confluence.plugins.smartforms.macros;

import java.util.Map;

import javax.inject.Inject;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.message.I18nResolver;

import de.edrup.confluence.plugins.smartforms.util.SafeJSONArray;
import de.edrup.confluence.plugins.smartforms.util.SafeJSONObject;


public class SmartFormsWorkflowMacro implements Macro {
	
	private final I18nResolver i18n;

	
	@Inject
	public SmartFormsWorkflowMacro(@ComponentImport I18nResolver i18n) {
		this.i18n = i18n;
	}

	
	@Override
	public String execute(Map<String, String> parameters, String bodyContent, ConversionContext conversionContext) throws MacroExecutionException {
		return checkWorkflow(bodyContent);
	}

	
	@Override
	public BodyType getBodyType() {
		return BodyType.PLAIN_TEXT;
	}

	
	@Override
	public OutputType getOutputType() {
		return OutputType.BLOCK;
	}
	
	
	// perform a formal workflow check
	private String checkWorkflow(String workflowAsString) {
		
		// JSON as such
		SafeJSONObject workflow = null;
		try {
			workflow = new SafeJSONObject(workflowAsString.replace("\u00a0",""));
		}
		catch(Exception e) {
			return wrapWorkflowCheckError(e.toString());
		}
		
		// top level values
		if(workflow.safeGetString("initialTransition", "smart_forms_oops").equals("smart_forms_oops")) return wrapWorkflowCheckError("initialState not defined");
		if(workflow.safeGetString("agentGroup", "smart_forms_oops").equals("smart_forms_oops")) return wrapWorkflowCheckError("agentGroup not defined");
		if(workflow.safeGetString("assignmentMailTitle", "smart_forms_oops").equals("smart_forms_oops")) return wrapWorkflowCheckError("assignmentMailTitle not defined");
		if(workflow.safeGetString("mailFromName", "smart_forms_oops").equals("smart_forms_oops")) return wrapWorkflowCheckError("mailFromName not defined");
		if(!workflow.checkOptionalForTypeString("dueDateFieldName")) return wrapWorkflowCheckError("dueDateFieldName is not of type string");
		if(!workflow.checkOptionalForTypeInt("initalDueDateInHoursFromNow")) return wrapWorkflowCheckError("initalDueDateInHoursFromNow is not of type int");
		if(!workflow.checkOptionalForTypeBoolean("restrictView")) return wrapWorkflowCheckError("restrictView is not of type boolean");
		
		// states
		SafeJSONArray states = workflow.safeGetJSONArray("states");
		if(states.length() == 0) return wrapWorkflowCheckError("states array not defined");
		for(int n = 0; n < states.length(); n++) {
			SafeJSONObject state = states.safeGetJSONObject(n);
			if(state.safeGetString("name", "smart_forms_oops").equals("smart_forms_oops")) return wrapWorkflowCheckError("name of state not defined");
			if(state.safeGetString("allowedAssignmentGroup", state.safeGetString("allowedAssignmentGroups", "smart_forms_oops")).equals("smart_forms_oops")) return wrapWorkflowCheckError("allowedAssignmentGroup of state not defined");
			if(!workflow.checkOptionalForTypeString("color")) return wrapWorkflowCheckError("color is not of type string");
			if(!workflow.checkOptionalForTypeString("textColor")) return wrapWorkflowCheckError("textColor is not of type string");
		}
		
		
		// transitions
		SafeJSONArray transitions = workflow.safeGetJSONArray("transitions");
		if(transitions.length() == 0) return wrapWorkflowCheckError("transitions array not defined");
		for(int n = 0; n < transitions.length(); n++) {
			SafeJSONObject tansition = transitions.safeGetJSONObject(n);
			if(tansition.safeGetString("name", "smart_forms_oops").equals("smart_forms_oops")) return wrapWorkflowCheckError("name of transition not defined");
			if(tansition.safeGetString("from", "smart_forms_oops").equals("smart_forms_oops")) return wrapWorkflowCheckError("from of transition not defined");
			if(tansition.safeGetString("to", "smart_forms_oops").equals("smart_forms_oops")) return wrapWorkflowCheckError("to of transition not defined");
			if(!workflow.checkOptionalForTypeString("enforceAssignmentTo")) return wrapWorkflowCheckError("enforceAssignmentTo is not of type string");
			if(!workflow.checkOptionalForTypeInt("increaseDueDateInHoursFromLastDueDate")) return wrapWorkflowCheckError("increaseDueDateInHoursFromLastDueDate is not of type int");
			// TODO: conditions
			// TODO: actions
		}
		
		return i18n.getText("de.edrup.confluence.plugins.smart-forms.workflow.ok");
	}
	
	
	// wrap the error message in nice aui message
	private String wrapWorkflowCheckError(String message) {
		return i18n.getText("de.edrup.confluence.plugins.smart-forms.workflow.error").replace("$message", message);
	}
}
