package de.edrup.confluence.plugins.smartforms.search;

import static com.atlassian.confluence.plugins.cql.spi.v2searchhelpers.V2FieldHandlerHelper.joinSingleValueQueries;
import static com.atlassian.confluence.plugins.cql.spi.v2searchhelpers.V2FieldHandlerHelper.wrapV2Search;
import static com.atlassian.querylang.fields.expressiondata.SetExpressionData.Operator.IN;
import static com.atlassian.querylang.fields.expressiondata.SetExpressionData.Operator.NOT_IN;
import static com.atlassian.querylang.fields.expressiondata.EqualityExpressionData.Operator.EQUALS;
import static com.atlassian.querylang.fields.expressiondata.EqualityExpressionData.Operator.NOT_EQUALS;
import static com.atlassian.querylang.fields.expressiondata.TextExpressionData.Operator.CONTAINS;
import static com.atlassian.querylang.fields.expressiondata.TextExpressionData.Operator.NOT_CONTAINS;
import static com.google.common.collect.Sets.newHashSet;

import com.atlassian.confluence.plugins.cql.spi.v2searchhelpers.V2SearchQueryWrapper;
import com.atlassian.confluence.plugins.cql.spi.v2searchhelpers.V2SearchSortWrapper;
import com.atlassian.confluence.search.v2.BooleanOperator;
import com.atlassian.confluence.search.v2.SearchQuery;
import com.atlassian.confluence.search.v2.SearchSort;
import com.atlassian.confluence.search.v2.query.TermQuery;
import com.atlassian.confluence.search.v2.query.TextFieldQuery;
import com.atlassian.confluence.search.v2.sort.FieldSort;
import com.atlassian.querylang.fields.BaseFieldHandler;
import com.atlassian.querylang.fields.EqualityFieldHandler;
import com.atlassian.querylang.fields.TextFieldHandler;
import com.atlassian.querylang.fields.expressiondata.EqualityExpressionData;
import com.atlassian.querylang.fields.expressiondata.SetExpressionData;
import com.atlassian.querylang.fields.expressiondata.TextExpressionData;
import com.atlassian.querylang.query.FieldOrder;
import com.atlassian.querylang.query.OrderDirection;
import com.google.common.base.Function;


public abstract class SmartFormsEqualityFieldHandler extends BaseFieldHandler implements EqualityFieldHandler<String, V2SearchQueryWrapper>, TextFieldHandler<V2SearchQueryWrapper> {
	
	private String FIELD_NAME = "";
	
    public SmartFormsEqualityFieldHandler(String fieldName) {
        super(fieldName, true);
        this.FIELD_NAME = fieldName;
    }

    @Override
    public V2SearchQueryWrapper build(SetExpressionData setExpressionData, Iterable<String> values) {
        validateSupportedOp(setExpressionData.getOperator(), newHashSet(IN, NOT_IN));
        SearchQuery query = joinSingleValueQueries(values, (Function<String, TermQuery>) this::createEqualityQuery);
        return wrapV2Search(query, setExpressionData);
    }

    @Override
    public V2SearchQueryWrapper build(EqualityExpressionData equalityExpressionData, String value) {
        validateSupportedOp(equalityExpressionData.getOperator(), newHashSet(EQUALS, NOT_EQUALS));
        return wrapV2Search(createEqualityQuery(value), equalityExpressionData);
    }
    
    @Override
    public V2SearchQueryWrapper build(TextExpressionData expressionData, String value) {
        validateSupportedOp(expressionData.getOperator(), newHashSet(CONTAINS, NOT_CONTAINS));
        return wrapV2Search(createTextQuery(value), expressionData);
    }

    private TermQuery createEqualityQuery(String value) {
        return new TermQuery(FIELD_NAME, value);
    }
    
    private TextFieldQuery createTextQuery(String value) {
    	return new TextFieldQuery(FIELD_NAME + "tx", value, BooleanOperator.OR);
    }
    
    public FieldOrder buildOrder(OrderDirection direction) {
    	return (FieldOrder)new V2SearchSortWrapper((SearchSort)new FieldSort(FIELD_NAME, SearchSort.Type.STRING, V2SearchSortWrapper.convertOrder(direction)));
    }
}

