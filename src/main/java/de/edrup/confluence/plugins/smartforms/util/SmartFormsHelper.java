package de.edrup.confluence.plugins.smartforms.util;

import static com.atlassian.confluence.mail.template.ConfluenceMailQueueItem.MIME_TYPE_HTML;

import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.TimeZone;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import javax.inject.Inject;
import javax.inject.Named;
import javax.naming.Context;
import javax.naming.NamingEnumeration;
import javax.naming.directory.InitialDirContext;
import javax.naming.directory.SearchControls;
import javax.xml.bind.DatatypeConverter;

import org.dhatim.fastexcel.Workbook;
import org.dhatim.fastexcel.Worksheet;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.cache.Cache;
import com.atlassian.cache.CacheManager;
import com.atlassian.cache.CacheSettingsBuilder;
import com.atlassian.confluence.api.model.Expansion;
import com.atlassian.confluence.api.model.pagination.SimplePageRequest;
import com.atlassian.confluence.api.model.search.SearchOptions;
import com.atlassian.confluence.api.model.search.SearchPageResponse;
import com.atlassian.confluence.api.model.search.SearchResult;
import com.atlassian.confluence.api.service.search.CQLSearchService;
import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.DefaultConversionContext;
import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.core.ContentPermissionManager;
import com.atlassian.confluence.core.ContentPropertyManager;
import com.atlassian.confluence.core.DefaultSaveContext;
import com.atlassian.confluence.core.SaveContext;
import com.atlassian.confluence.jmx.JmxSMTPMailServer;
import com.atlassian.confluence.labels.Label;
import com.atlassian.confluence.labels.LabelManager;
import com.atlassian.confluence.mail.notification.Notification;
import com.atlassian.confluence.mail.notification.NotificationManager;
import com.atlassian.confluence.mail.template.ConfluenceMailQueueItem;
import com.atlassian.confluence.pages.Attachment;
import com.atlassian.confluence.pages.AttachmentManager;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.pages.PageUpdateTrigger;
import com.atlassian.confluence.renderer.radeox.macros.MacroUtils;
import com.atlassian.confluence.search.ConfluenceIndexer;
import com.atlassian.confluence.security.ContentPermission;
import com.atlassian.confluence.security.ContentPermissionSet;
import com.atlassian.confluence.security.Permission;
import com.atlassian.confluence.security.PermissionManager;
import com.atlassian.confluence.setup.settings.SettingsManager;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.user.ConfluenceUser;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.confluence.util.HtmlUtil;
import com.atlassian.confluence.util.velocity.VelocityUtils;
import com.atlassian.confluence.xhtml.api.MacroDefinition;
import com.atlassian.confluence.xhtml.api.MacroDefinitionHandler;
import com.atlassian.confluence.xhtml.api.XhtmlContent;
import com.atlassian.core.task.MultiQueueTaskManager;
import com.atlassian.crowd.manager.directory.DirectoryManager;
import com.atlassian.json.jsonorg.JSONArray;
import com.atlassian.json.jsonorg.JSONObject;
import com.atlassian.mail.MailFactory;
import com.atlassian.mail.server.MailServerManager;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.message.I18nResolver;
import com.atlassian.sal.api.transaction.TransactionTemplate;
import com.atlassian.sal.api.user.UserKey;
import com.atlassian.user.GroupManager;
import com.atlassian.user.search.query.EmailTermQuery;
import com.atlassian.user.search.query.FullNameTermQuery;
import com.atlassian.confluence.importexport.resource.DownloadResourceWriter;
import com.atlassian.confluence.importexport.resource.WritableDownloadResourceManager;


@Named
public class SmartFormsHelper {
	
	private final PageManager pageMan;
	private final XhtmlContent xhtmlUtils;
	private final ContentPropertyManager propertyMan;
	private final UserAccessor userAcc;
	private final ContentPermissionManager contentPermissionMan;
	private final NotificationManager notificationMan;
	private final I18nResolver i18n;
	private final CQLSearchService searchService;
	private final SettingsManager settingsMan;
	private final MultiQueueTaskManager mqtm;
	private final SpaceManager spaceMan;
	private final PermissionManager permissionMan;
	private final GroupManager groupMan;
	private final DirectoryManager directoryMan;
	private final WritableDownloadResourceManager downloadResourceMan;
	private final ConfluenceIndexer confluenceIndexer;
	private final TransactionTemplate transactionTemplate;
	private final AttachmentManager attachmentMan;
	private final LabelManager labelMan;
	
	private final Cache<String, String> countryCache;
	private final Cache<String, Map<String, String>> macroParamCache; 
	
	private static final Logger log = LoggerFactory.getLogger(SmartFormsHelper.class);
	
	private final int BATCH_LIMIT = 250;
	
	
	@Inject
	public SmartFormsHelper(@ComponentImport PageManager pageMan, @ComponentImport XhtmlContent xhtmlUtils,
		@ComponentImport ContentPropertyManager propertyMan, @ComponentImport UserAccessor userAcc,
		@ComponentImport ContentPermissionManager contentPermissionMan, @ComponentImport NotificationManager notificationMan,
		@ComponentImport I18nResolver i18n, @ComponentImport CQLSearchService searchService, @ComponentImport SettingsManager settingsMan,
		@ComponentImport MultiQueueTaskManager mqtm, @ComponentImport SpaceManager spaceMan, @ComponentImport PermissionManager permissionMan,
		@ComponentImport GroupManager groupMan, @ComponentImport DirectoryManager directoryMan, @ComponentImport CacheManager cacheMan,
		@ComponentImport WritableDownloadResourceManager downloadResourceMan, @ComponentImport ConfluenceIndexer confluenceIndexer,
		@ComponentImport TransactionTemplate transactionTemplate, @ComponentImport AttachmentManager attachmentMan, @ComponentImport LabelManager labelMan) {
		this.pageMan = pageMan;
		this.xhtmlUtils = xhtmlUtils;
		this.propertyMan = propertyMan;
		this.userAcc = userAcc;
		this.contentPermissionMan = contentPermissionMan;
		this.notificationMan = notificationMan;
		this.i18n = i18n;
		this.searchService = searchService;
		this.settingsMan = settingsMan;
		this.mqtm = mqtm;
		this.spaceMan = spaceMan;
		this.permissionMan = permissionMan;
		this.groupMan = groupMan;
		this.directoryMan = directoryMan;
		this.downloadResourceMan = downloadResourceMan;
		this.confluenceIndexer = confluenceIndexer;
		this.transactionTemplate = transactionTemplate;
		this.attachmentMan = attachmentMan;
		this.labelMan = labelMan;
		
		countryCache = cacheMan.getCache("Smart Forms country cache",  null,
			new CacheSettingsBuilder().remote().expireAfterWrite(30, TimeUnit.DAYS).build());
		macroParamCache = cacheMan.getCache("Smart Forms macro parameter cache", null,
			new CacheSettingsBuilder().remote().expireAfterWrite(3, TimeUnit.SECONDS).build());
	}
	
	
	// create a page under the parent page with parentPageId using the given key - the macro has to refer back to the create page supplying the details
	public Long createPage(Long formPageId, Long workflowPageId, Long parentPageId, String key) {
		
		Page parentPage = pageMan.getPage(parentPageId);
		
		Page p = new Page();
		p.setTitle(getNextPageName(parentPage, key));
		p.setCreator(AuthenticatedUserThreadLocal.get());
		p.setCreationDate(new Date());
		String body = String.format("<p><ac:structured-macro ac:name=\"smart-forms-instance\" ac:schema-version=\"3\">"
				+ "<ac:parameter ac:name=\"createPage\">0</ac:parameter>"
				+ "<ac:parameter ac:name=\"formPageId\">%d</ac:parameter>"
				+ "<ac:parameter ac:name=\"workflowPageId\">%d</ac:parameter>"
				+ "</ac:structured-macro></p>", formPageId, workflowPageId);
		p.setBodyAsString(body);
		p.setSpace(parentPage.getSpace());		
		p.setParentPage(parentPage);
		parentPage.addChild(p);
		
		pageMan.saveContentEntity(p, null, buildSaveContext());
		
		handleRestrictions(p.getId());
		
		if(isWorkflowDefined(p.getId())) {
			SafeJSONObject workflow = getApplicableWorkflow(p.getId());
			executeTransition(p.getId(), workflow.safeGetString("initialTransition", ""));
		}
		
		return p.getId();
	}
	
	
	// get next name for the page to be created
	private String getNextPageName(Page parentPage, String key) {
		Page startPage = parentPage.getParent() != null ? parentPage.getParent() : parentPage;
		List<String> titles = pageMan.getDescendantTitles(startPage);
		Integer maxNumber = 0;
		for(String title : titles) {
			if(title.startsWith(key.concat("-"))) {
				Integer number = Integer.parseInt(title.replace(key + "-", ""));
				maxNumber = (number > maxNumber) ? number : maxNumber;
			}
		}
		
		maxNumber++;
		return key + "-" + maxNumber.toString();
	}
	
	
	// the the pageId of the applicable form
	public Long getApplicableFormPageId(Long pageId) {
		try {
			Map<String, String> instanceParameters = getParametersOfMacro(pageId, "smart-forms-instance");
			if(instanceParameters.containsKey("formPageId") && instanceParameters.get("formPageId").length() > 1) {
				return Long.parseLong(instanceParameters.get("formPageId"));
			}
			// legacy 
			else {
				Map<String, String> parameters = getParametersOfMacro(Long.parseLong(getParametersOfMacro(pageId, "smart-forms-instance").get("createPage")), "smart-forms-create");
				return getPageIdFromPageTitle(parameters.get("formPage"),  pageMan.getPage(pageId).getSpaceKey());
			}
		}
		catch(Exception e) {
			log.debug(e.toString());
			log.debug(getStackTrace(e));
			return 0L;
		}		
	}
	
	
	// get the applicable form for the provided pageId
	public String getApplicableForm(Long pageId) {
		try {
			return getBodyContentOfMacro(getApplicableFormPageId(pageId), "smart-forms-form");
		}
		catch(Exception e) {
			log.debug(e.toString());
			log.debug(getStackTrace(e));
			return e.toString();
		}
	}
	
	
	// get the applicable workflow for the provided pageId
	public SafeJSONObject getApplicableWorkflow(Long pageId) {
		try {
			Map<String, String> instanceParameters = getParametersOfMacro(pageId, "smart-forms-instance");
			if(instanceParameters.containsKey("workflowPageId") && instanceParameters.get("workflowPageId").length() > 1) {
				return new SafeJSONObject(getBodyContentOfMacro(Long.parseLong(instanceParameters.get("workflowPageId")), "smart-forms-workflow").replace("\u00a0",""));
			}
			// legacy
			else { 
				Map<String, String> parameters = getParametersOfMacro(Long.parseLong(getParametersOfMacro(pageId, "smart-forms-instance").get("createPage")), "smart-forms-create");
				if(parameters.get("workflowPage") != null) {
					return new SafeJSONObject(getBodyContentOfMacro(getPageIdFromPageTitle(parameters.get("workflowPage"),  pageMan.getPage(pageId).getSpaceKey()), "smart-forms-workflow").replace("\u00a0",""));
				}
				else {
					return null;
				}
			}
		}
		catch(Exception e) {
			log.debug(e.toString());
			log.debug(getStackTrace(e));
			return null;
		}
	}
	
	
	// get the applicable workflow page id for the provided pageId
	public Long getApplicableWorkflowPageId(Long pageId) {
		try {
			Map<String, String> instanceParameters = getParametersOfMacro(pageId, "smart-forms-instance");
			if(instanceParameters.containsKey("workflowPageId") && instanceParameters.get("workflowPageId").length() > 1) {
				return Long.parseLong(instanceParameters.get("workflowPageId"));
			}
			else {
				Map<String, String> parameters = getParametersOfMacro(Long.parseLong(getParametersOfMacro(pageId, "smart-forms-instance").get("createPage")), "smart-forms-create");
				if(parameters.get("workflowPage") != null) {
					return getPageIdFromPageTitle(parameters.get("workflowPage"),  pageMan.getPage(pageId).getSpaceKey());
				}
				else {
					return null;
				}
			}
		}
		catch(Exception e) {
			log.debug(e.toString());
			log.debug(getStackTrace(e));
			return null;
		}
	}
	
	
	// get the macro parameters of the first macro matching the key on the given page
	private Map<String, String> getParametersOfMacro(Long pageId, String macroKey) {
		
		String cacheKey = pageId.toString().concat(macroKey);
		Map<String, String> paramsFromCache = macroParamCache.get(cacheKey);
		if(paramsFromCache != null) {
			return paramsFromCache;
		}

		Page page = pageMan.getPage(pageId);
		final HashMap<String, String> parameters = new HashMap<String, String>();
    	
    	try
		{
			xhtmlUtils.handleMacroDefinitions(page.getBodyAsString(), new DefaultConversionContext(page.toPageContext()), new MacroDefinitionHandler()
			{
				@Override
				public void handle(MacroDefinition macroDefinition) {
					if(macroDefinition.getName().equals(macroKey)) {
						Set<String> parameterNames = macroDefinition.getParameters().keySet();
						for(String parameterName : parameterNames) {
							parameters.put(parameterName, macroDefinition.getParameters().get(parameterName));
						}
					}
				}
			});
		}
        catch (Exception e) {
        	log.error(e.toString());
        }
    	
    	macroParamCache.put(cacheKey, parameters);
    	
    	return parameters;
	}
	
	
	// get the content body of the first macro matching the key or macro-id on the given page
	private String getBodyContentOfMacro(Long pageId, String macroKeyOrId) {
		
		Page page = pageMan.getPage(pageId);
		final ArrayList<String> bodyContent = new ArrayList<String>();
		
		log.debug("Getting content of macro {} on page {}", macroKeyOrId, pageId);
		
		try
		{
			xhtmlUtils.handleMacroDefinitions(page.getBodyAsString(), new DefaultConversionContext(page.toPageContext()), new MacroDefinitionHandler()
			{
				@Override
				public void handle(MacroDefinition macroDefinition) {
					if(macroDefinition.getName().equals(macroKeyOrId) || macroKeyOrId.equals(macroDefinition.getMacroIdentifier().get().getId())) {
						try {
							StringWriter sw = new StringWriter();
							macroDefinition.getBody().getStorageBodyStream().writeTo(sw);
							bodyContent.add(sw.toString());
							sw.close();
						}
						catch(Exception e) {
							log.error(e.toString());
						}
						return;
					}
				}
			});
		}
        catch (Exception e) {
        	log.error(e.toString());
        }
  
    	return (bodyContent.size() > 0) ? bodyContent.get(0) : "";
	}
	
	
	// get the rendered macro on the given page
	public String getRenderedMacro(Long pageId, String macroKeyOrId) {
		String body = getBodyContentOfMacro(pageId, macroKeyOrId);
		try {
			return xhtmlUtils.convertStorageToView(body, new DefaultConversionContext(pageMan.getPage(pageId).toPageContext()));
			
		}
		catch(Exception e) {
			return e.toString();
		}
	}
	
	
	// get the page id from the given page title and space key
	public Long getPageIdFromPageTitle(String title, String spaceKey) {
		
		log.debug("Getting page id for title {} in space {}", title, spaceKey);
		
		if(title.contains(":")) {
			spaceKey = title.substring(0, title.indexOf(":"));
			title = title.substring(title.indexOf(":") + 1);
		}
		return pageMan.getPage(spaceKey, title).getId();
	}
	
	
	// get the current form data stored as content property
	public String getCurrentFormData(Long pageId) {
		String currentData = propertyMan.getTextProperty(pageMan.getPage(pageId), "smart-forms-form-data");
		return (currentData == null) ? "{}" : currentData;
	}
	
	
	// store the form data as text property
	public int setFormData(Long pageId, String data) {
		
		// in case the form data changed
		if(!data.equals(getCurrentFormData(pageId))) {
			log.debug("Setting form data on page {} to {}", pageId, data);
			propertyMan.setTextProperty(pageMan.getPage(pageId), "smart-forms-form-data", data);
			createNewPageVersion(pageId, i18n.getText("de.edrup.confluence.plugins.smart-forms.versionComment.formChange"));
		}
		
		return pageMan.getPage(pageId).getVersion();
	}
	
	
	// get the state saved as content property
	private String getSavedState(Long pageId) {
		return propertyMan.getTextProperty(pageMan.getPage(pageId), "smart-forms-workflow-state");
	}
	
	
	// get the state saved as content property
	private String getPrevState(Long pageId) {
		return propertyMan.getTextProperty(pageMan.getPage(pageId), "smart-forms-workflow-state-prev");
	}
	
	
	// set the state as content property
	private void setState(Long pageId, String state) {
		if(getSavedState(pageId) != null) {
			propertyMan.setTextProperty(pageMan.getPage(pageId), "smart-forms-workflow-state-prev", getSavedState(pageId));
		}
		propertyMan.setTextProperty(pageMan.getPage(pageId), "smart-forms-workflow-state", state);
	}
	
	
	// check if a workflow is defined for the given page id
	public boolean isWorkflowDefined(Long pageId) {
		return (getApplicableWorkflow(pageId) == null) ? false : true;
	}
	
	
	// get the name of the current state
	public String getCurrentStateName(Long pageId) {
		String currentState = getSavedState(pageId);

		return currentState != null ? currentState : "";
	}
	
	
	// get the background color of the current state
	public String getCurrentStateColor(Long pageId) {
		return getCurrentState(pageId).safeGetString("color", "#F97781");
	}

	
	// get the text color of the current state
	public String getCurrentStateTextColor(Long pageId) {
		return getCurrentState(pageId).safeGetString("textColor", "black");
	}
	
	
	// get the tooltips the current state
	public String getCurrentStateTooltip(Long pageId) {
		return getCurrentState(pageId).safeGetString("tooltip", "");
	}
	
	
	// get the current state
	public SafeJSONObject getCurrentState(Long pageId) {
		String currentState = getCurrentStateName(pageId);
		SafeJSONObject workflow = getApplicableWorkflow(pageId);
		for(int n = 0; n < workflow.getJSONArray("states").length(); n++) {
			SafeJSONObject state =  workflow.safeGetJSONArray("states").safeGetJSONObject(n);
			if(state.safeGetString("name","name_not_found").equals(currentState)) {
				return state;
			}
		}
		return new SafeJSONObject();
	}

	
	// get the name of all possible transitions from the current state of the given page id
	public List<JSONObject> getTransitions(Long pageId) {
		String currentState = getCurrentStateName(pageId);
		SafeJSONObject workflow = getApplicableWorkflow(pageId);
		ArrayList<String> groupNames = new ArrayList<String>();
		groupNames.addAll(userAcc.getGroupNames(AuthenticatedUserThreadLocal.get()));
		if(pageMan.getPage(pageId).getCreator().equals(AuthenticatedUserThreadLocal.get())) {
			groupNames.add("creator-group");
		}
		
		ArrayList<JSONObject> transitions = new ArrayList<JSONObject>();
		
		for(int n = 0; n < workflow.getJSONArray("transitions").length(); n++) {
			SafeJSONObject transition =  workflow.safeGetJSONArray("transitions").safeGetJSONObject(n);
			List<String> allowedGroups = Arrays.asList(getAllowedGroupsForTransition(transition).split(" *, *"));
			if(Arrays.asList(transition.safeGetString("from", "from_not_defined").split(" *, *")).contains(currentState) && !Collections.disjoint(groupNames, allowedGroups) && !transition.safeGetBoolean("hidden", false)) {
				transitions.add(transition);
			}
		}
		
		return transitions;
	}
	
	
	// execute the provided transition
	private String executeTransition(Long pageId, String transitionName) {
		
		JSONObject response = new JSONObject();

		try {
			String currentState = getCurrentStateName(pageId);
			SafeJSONObject workflow = getApplicableWorkflow(pageId);
			
			for(int n = 0; n < workflow.getJSONArray("transitions").length(); n++) {
				SafeJSONObject transition =  workflow.safeGetJSONArray("transitions").safeGetJSONObject(n);
				if((Arrays.asList(transition.safeGetString("from", "from_not_defined").split(" *, *")).contains(currentState) || currentState.isEmpty()) && transition.safeGetString("name", "name_not_found").equals(transitionName)) {
					
					// check conditions
					String conditionsFeedback = checkConditions(pageId, transition.safeGetJSONArray("conditions"));
					if(!conditionsFeedback.isEmpty()) {
						response.put("success", false);
						response.put("details", conditionsFeedback);
						return response.toString();
					}
					
					// set new state
					String newState = transition.safeGetString("to", "to_not_found");
					if(newState.equals("$previousState")) {
						newState = getPrevState(pageId);
					}
					setState(pageId, newState);
					
					// create new page version
					createNewPageVersion(pageId, i18n.getText("de.edrup.confluence.plugins.smart-forms.versionComment.transition").replace("$stateName", transition.safeGetString("to", "to_not_found")));

					// execute post actions
					executePostActions(pageId, transition.safeGetJSONArray("postActions"));
					
					response.put("success", true);
					response.put("details", "");
					return response.toString();
				}
			}			
		}
		catch(Exception e) {
			log.error(e.toString());
			log.debug(getStackTrace(e));			
		}
		
		response.put("success", false);
		response.put("details", "not provided");
		return response.toString();
	}
	
	
	// execute the provided transition in a single transaction
	public String executeTransitionTransactional(Long pageId, String transitionName) {
		return transactionTemplate.execute(() -> executeTransition(pageId, transitionName));
	}
	
	
	// find a transition between two states and execute it
	private String findAndExecuteTransition(Long pageId, String to) { 
		
		try {
			String currentState = getCurrentStateName(pageId);
			SafeJSONObject workflow = getApplicableWorkflow(pageId);
			
			for(int n = 0; n < workflow.getJSONArray("transitions").length(); n++) {
				SafeJSONObject transition =  workflow.safeGetJSONArray("transitions").safeGetJSONObject(n);
				if(transition.safeGetString("to", "to_not_found").equals(to) && Arrays.asList(transition.safeGetString("from", "from_not_defined").split(" *, *")).contains(currentState)) {
					return executeTransition(pageId, transition.safeGetString("name", "name_not_found"));
				}
			}
		}
		catch(Exception e) {
			log.error(e.toString());
			log.debug(getStackTrace(e));			
		}
		
		JSONObject response = new JSONObject();
		response.put("success", false);
		response.put("details", i18n.getText("de.edrup.confluence.plugins.smart-forms.overview.transitionNotFound"));
		return response.toString();
	}
	
	
	// find a transition between two states and execute it
	public String findAndExecuteTransitionTransactional(Long pageId, String to) { 
		return transactionTemplate.execute(() -> findAndExecuteTransition(pageId, to));
	}
	
	
	// check conditions
	private String checkConditions(Long pageId, SafeJSONArray conditions) {
		
		String conditionsFeedback = "";
		ConfluenceUser user = AuthenticatedUserThreadLocal.get();
		SafeJSONObject currentFormData = new SafeJSONObject(getCurrentFormData(pageId));
		ArrayList<String> currentUsersGroups = new ArrayList<String>();
		currentUsersGroups.addAll(userAcc.getGroupNames(user));
		if(pageMan.getPage(pageId).getCreator().equals(user)) {
			currentUsersGroups.add("creator-group");
		}
		
		for(int n = 0; n < conditions.length(); n++) {
			SafeJSONObject condition = conditions.safeGetJSONObject(n);
			switch(condition.safeGetString("type", "")) {
				case "requireField" :
					if(currentFormData.safeGetString(condition.safeGetString("value", "value_not_found"), "").replace("select2:", "").isEmpty()) {
						conditionsFeedback += i18n.getText("de.edrup.confluence.plugins.smart-forms.form.transition.requiredParamter") + condition.safeGetString("value", "value_not_found") + "\n";
					}
					break;
				case "allowedGroup" :
					if(!currentUsersGroups.contains(condition.safeGetString("value", ""))) {
						conditionsFeedback += i18n.getText("de.edrup.confluence.plugins.smart-forms.form.transition.notAllowed") + condition.safeGetString("value", "value_not_found") + "\n";
					}
					break;
				case "allowedGroups" :
					List<String> allowedGroups = Arrays.asList(condition.safeGetString("value", "").split(" *, *"));
					if(Collections.disjoint(currentUsersGroups, allowedGroups)) {
						conditionsFeedback += i18n.getText("de.edrup.confluence.plugins.smart-forms.form.transition.notAllowed") + condition.safeGetString("value", "value_not_found") + "\n";
					}
					break;
				case "subInstancesStatus" :
					if(!checkSubStatus(pageId, condition.safeGetString("value", "value_not_found"))) {
						conditionsFeedback += i18n.getText("de.edrup.confluence.plugins.smart-forms.form.transition.subStatus") +  "\n";						
					}
					break;
				case "requireValue" :
					if(!currentFormData.safeGetString(condition.safeGetString("field", "field_not_found"), "").equals(condition.safeGetString("value", "value_not_found"))) {
						conditionsFeedback += i18n.getText("de.edrup.confluence.plugins.smart-forms.form.transition.requiredValue") + condition.safeGetString("field", "field_not_found") + "\n";
					}
					break;
				case "valueContains" :
					if(!currentFormData.safeGetString(condition.safeGetString("field", "field_not_found"), "").contains(enrichValueCondition(condition.safeGetString("value", "value_not_found"), user))) {
						conditionsFeedback += i18n.getText("de.edrup.confluence.plugins.smart-forms.form.transition.valueContains")
							.replace("[1]", condition.safeGetString("field", "field_not_found")).replace("[2]", condition.safeGetString("value", "value_not_found")) + "\n";
					}
					break;
				case "valueNotContains" :
					String fieldValue = currentFormData.safeGetString(condition.safeGetString("field", "field_not_found"), "");
					String notContains = enrichValueCondition(condition.safeGetString("value", "value_not_found"), user);
					if(fieldValue.contains(notContains)) {
						if(fieldValue.contains("multiple:") && notContains.equals(",")) {
							notContains = "multiple values";
						}
						conditionsFeedback += i18n.getText("de.edrup.confluence.plugins.smart-forms.form.transition.valueNotContains")
							.replace("[1]", condition.safeGetString("field", "field_not_found")).replace("[2]", notContains) + "\n";
					}
					break;
				case "freeOfLinkedInstances" :
					if(!isFreeOfLinkedInstances(pageId, condition.safeGetString("field", "field_not_found"))) {
						conditionsFeedback += condition.safeGetString("message", "Not free of linked instances");
					}
					break;
				default:
			}
		}
		
		return conditionsFeedback;
	}
	
	
	// enrich a value condition with user related placeholders
	private String enrichValueCondition(String value, ConfluenceUser user) {
		if(value.contains("$currentUser")) value = value.replace("$currentUser", user.getName());
		if(value.contains("$currentUserKey")) value = value.replace("$currentUserKey", user.getKey().getStringValue());
		return value;
	}
	
	
	// check the status of sub instances
	boolean checkSubStatus(Long pageId, String allowedStatus) {
		List<Long> childPageIds = pageMan.getDescendants(pageMan.getPage(pageId)).stream().map(x -> x.getId()).collect(Collectors.toList());
		for(Long childPageId : childPageIds) {
			if(!allowedStatus.contains(getCurrentStateName(childPageId))) {
				return false;
			}
		}
		return true;
	}
	
	
	// check whether there are any linked instances
	boolean isFreeOfLinkedInstances(Long pageId, String linkField) {
		SmartFormsPageResult pageResult = getPagesMatchingQuery("smartformsdata=\"" + linkField + ":" + pageId.toString() + "\"", 0, 1);
		return pageResult.getTotalSize() == 0;
	}
	
	
	// execute post functions
	private void executePostActions(Long pageId, SafeJSONArray postActions) {
		
		ConfluenceUser user = AuthenticatedUserThreadLocal.get();
		
		for(int n = 0; n < postActions.length(); n++) {
			SafeJSONObject postAction = postActions.safeGetJSONObject(n);
			switch(postAction.safeGetString("type", "")) {
				case "setDueDateInWorkingHoursFromNow" :
					setDueDate(pageId, false, postAction.safeGetInt("value", 0));
					break;
				case "setDueDateInWorkingHoursFromLastDueDate" :
					setDueDate(pageId, true, postAction.safeGetInt("value", 0));
					break;
				case "assignToIfUnassigned":
				case "enforceAssignmentTo":
					if((postAction.safeGetString("type", "").equals("assignToIfUnassigned") && (getAssignedUser(pageId) == null)) || postAction.safeGetString("type", "").equals("enforceAssignmentTo")) {
						switch(postAction.safeGetString("value", "")) {
							case "$currentUser" :
								assignUser(pageId, AuthenticatedUserThreadLocal.getUsername());
								break;
							case "$creator" :
								assignUser(pageId, pageMan.getPage(pageId).getCreator().getName());
								break;
							case "none" :
								assignUser(pageId, null);
								break;
							case "":
								break;
							default:
								assignUser(pageId, postAction.safeGetString("value", ""));
						}
					}
					break;
				case "assignToIfFieldContains":
					String fieldName = postAction.safeGetString("fieldName", "");
					String shouldContain = postAction.safeGetString("shouldContain", "");
					SafeJSONObject currentFormData = new SafeJSONObject(getCurrentFormData(pageId));
					if(currentFormData.safeGetString(fieldName, "unknownField").contains(shouldContain)) {
						assignUser(pageId, postAction.safeGetString("value", ""));
					}
					break;
				case "assignToUserFromField":
					String userName = new SafeJSONObject(getCurrentFormData(pageId)).safeGetString(postAction.safeGetString("fieldName", ""), null);
					assignUser(pageId, userName);
					break;
				case "setFieldValue" :
				case "setFieldValueIfEmpty" :
					boolean ifEmpty = postAction.safeGetString("type", "").equals("setFieldValueIfEmpty");
					setFieldValue(pageId, postAction.safeGetString("fieldName", ""), enrichFieldValue(pageId, postAction, user), !ifEmpty);
					break;
				case "notifyGroup" :
					notifyGroup(pageId, null, postAction.safeGetString("group", ""), postAction.safeGetString("title", "no title defined in postAction"), postAction.safeGetString("message", "no message defined in postAction"));
					break;
				case "notifyGroupLinked" :
					notifyGroup(pageId, getLinkedInstance(pageId, postAction.safeGetString("linkField", "no link field defined")), postAction.safeGetString("group", ""), postAction.safeGetString("title", "no title defined in postAction"), postAction.safeGetString("message", "no message defined in postAction"));
					break;
				case "unwatchAll" :
					removeAllWatches(pageId);
					break;
				case "notifyUserFromField":
					notifyUserFromField(pageId,  null, postAction.safeGetString("fieldName", ""), postAction.safeGetString("title", "no title defined in postAction"), postAction.safeGetString("message", "no message defined in postAction"));
					break;
				case "notifyUserFromFieldLinked":
					notifyUserFromField(pageId, getLinkedInstance(pageId, postAction.safeGetString("linkField", "no link field defined")), postAction.safeGetString("fieldName", ""), postAction.safeGetString("title", "no title defined in postAction"), postAction.safeGetString("message", "no message defined in postAction"));
					break;
				case "notifyEmail":
					notifyEmail(pageId, postAction.safeGetString("email", ""), postAction.safeGetString("title", "no title defined in postAction"), postAction.safeGetString("message", "no message defined in postAction"));
					break;
				case "countUp" :
					countUp(pageId, postAction.safeGetString("fieldName", ""));
					break;
				case "executeTransitionLinked" :
					Long linkedPageId = getLinkedInstance(pageId, postAction.safeGetString("linkField", "no link field defined"));
					if(linkedPageId != null) {
						executeTransition(linkedPageId, postAction.safeGetString("transitionName", "no transition name defined"));
					}
					break;
				case "applyEditPermission":
				case "applyViewPermission":
				case "removeEditPermission":
				case "removeViewPermission":
					handlePermissionPostAction(pageId,  postAction.safeGetString("type", ""), postAction.safeGetString("groupOrUser", "field:" + postAction.safeGetString("fieldName", "")));
					break;
				case "reIndex":
					reIndexPage(pageId);
					break;
				case "changeCreator":
					changeCreator(pageId, postAction.safeGetString("fieldName", ""));
					break;
				default:
			}
		}
	}
	
	
	// enrich the field value by placeholders
	private String enrichFieldValue(Long pageId, SafeJSONObject postAction, ConfluenceUser user) {
		String toValue = postAction.safeGetString("value", "");
		String fieldName = postAction.safeGetString("fieldName", "");
		if(toValue.contains("$currentValue")) {
			SafeJSONObject currentFormData = new SafeJSONObject(getCurrentFormData(pageId));
			toValue = toValue.replace("$currentValue", currentFormData.safeGetString(fieldName, "unknownField"));
		}
		if(toValue.contains("$currentUserKey")) toValue = toValue.replace("$currentUserKey", user.getKey().getStringValue());
		if(toValue.contains("$currentUserPlus")) toValue = toValue.replace("$currentUserPlus", getCleanedFullName(user.getFullName()) + " (" + user.getEmail() + ")");
		if(toValue.contains("$currentUser")) toValue = toValue.replace("$currentUser", user.getName());
		if(toValue.contains("$currentUserEmail")) toValue = toValue.replace("$currentUserEmail", user.getEmail());
		if(toValue.contains("$currentDateTime")) toValue = toValue.replace("$currentDateTime", getInISO8601UTC(new Date()));
		if(toValue.contains("$adValue")) toValue = toValue.replace("$adValue", getAttributeFromADForUser(user.getName(), postAction.safeGetString("attribute", "sn")));
		return toValue;
	}
	
	
	// get the cleaned full name
	public String getCleanedFullName(String fullName) {
		return fullName.replace(",", " ").replaceAll(" +", " ").replaceAll("\\(.*?\\)", "");
	}
	
	
	// count up a field value
	private void countUp(Long pageId, String fieldName) {
		Integer count = 0;
		SafeJSONObject currentFormData = new SafeJSONObject(getCurrentFormData(pageId));
		String currentCount = currentFormData.safeGetString(fieldName, "");
		count = Integer.parseInt(currentCount.isEmpty() ? "0" : currentCount) + 1;
		currentFormData.put(fieldName, count.toString());
		setFormData(pageId, currentFormData.toString());
	}
	
	
	// set a field
	public void setFieldValue(Long pageId, String fieldName, String value, boolean overwrite) {
		SafeJSONObject currentFormData = new SafeJSONObject(getCurrentFormData(pageId));
		if(overwrite || currentFormData.safeGetString(fieldName, "").equals("")) {
			currentFormData.put(fieldName, value);
		}
		setFormData(pageId, currentFormData.toString());
	}
	
	
	// remove all watches
	private void removeAllWatches(Long pageId) {
		for(Notification notification : notificationMan.getNotificationsByContent(pageMan.getPage(pageId))) {
			notificationMan.removeNotification(notification);
		}
	}
	
	
	// get allowed group for the transition
	private String getAllowedGroupsForTransition(SafeJSONObject transition) {
		SafeJSONArray conditions = transition.safeGetJSONArray("conditions");
		for(int n = 0; n < conditions.length(); n++) {
			SafeJSONObject condition = conditions.safeGetJSONObject(n);
			if(condition.safeGetString("type", "").equals("allowedGroup") || condition.safeGetString("type", "").equals("allowedGroups")) {
				return condition.safeGetString("value", "");
			}
		}
		return "";
	}
	
	
	// render an attachment section to be included
	public String renderAttachmentSection(Long pageId) {
		try {
			return xhtmlUtils.convertStorageToView(" <ac:structured-macro ac:name=\"attachments\" ac:schema-version=\"1\"/>", new DefaultConversionContext(pageMan.getPage(pageId).toPageContext()));
		}
		catch(Exception e) {
			log.error(e.toString());
			log.debug(getStackTrace(e));
		}
		
		return "";
	}
	
	
	// render the rendered section
	public String renderRenderedSection(Long pageId) {
		try {
			Long formPageId = getApplicableFormPageId(pageId);
			ConversionContext conversionContext = new DefaultConversionContext(pageMan.getPage(pageId).toPageContext());
			conversionContext.setProperty("formPageId", formPageId);
			String rendered = xhtmlUtils.convertStorageToView(getBodyContentOfMacro(formPageId, "smart-forms-rendered-section"), conversionContext);
			rendered = rendered.replaceAll("overview-template-page-id=\".*?\"", String.format("overview-template-page-id=\"%d\"", formPageId));
			return rendered;
		}
		catch(Exception e) {
			log.error(e.toString());
			log.debug(getStackTrace(e));
		}
		
		return "";
	}
	
	
	// restrict the users allowed to view a form instance if specified
	private void handleRestrictions(Long pageId) {
		try {
			SafeJSONObject workflow = getApplicableWorkflow(pageId);
			if(workflow != null) {
				if(workflow.safeGetBoolean("restrictView", false) || workflow.safeGetBoolean("restrictEdit", false)) {
					applyUserEditPermission(pageMan.getPage(pageId), AuthenticatedUserThreadLocal.get());
					applyGroupEditPermission(pageMan.getPage(pageId), workflow.getString("agentGroup"));
					if(workflow.safeGetBoolean("restrictView", false)) {
						applyUserViewPermission(pageMan.getPage(pageId), AuthenticatedUserThreadLocal.get());
						applyGroupViewPermission(pageMan.getPage(pageId), workflow.getString("agentGroup"));
					}
				}
			}
		}
		catch(Exception e) {
			log.error(e.toString());
			log.debug(getStackTrace(e));
		}
	}
	
	
	// handle permission post action
	private void handlePermissionPostAction(Long pageId, String actionName, String groupOrUser) {
		if(groupOrUser.startsWith("field:")) {
			String fieldValue = getAllFormData(pageId).getString(groupOrUser.replace("field:", "")).replace("select2:", "").replace("multiple:", "");
			String[] singleValues = fieldValue.split(" *, *");
			for(String singleValue : singleValues) {
				if(singleValue.length() > 0) {
					singleValue = singleValue.endsWith(")") ? singleValue.replaceFirst(".*?\\(", "").replace(")", "") : singleValue;
					handleSinglePermission(pageId, actionName, singleValue);
				}
			}
		}
		else {
			handleSinglePermission(pageId, actionName, groupOrUser);
		}
	}
	
	
	// handle a single permission change (single user or group)
	private void handleSinglePermission(Long pageId, String actionName, String groupOrUser) {
		try {
			if(groupOrUser.contains("@")) groupOrUser = findUserByEmail(groupOrUser);
			ConfluenceUser userOrNull = userAcc.getUserByName(groupOrUser);
			if(groupOrUser.equals("$currentUser")) userOrNull =  AuthenticatedUserThreadLocal.get();
			if(groupOrUser.equals("$creator")) userOrNull = pageMan.getPage(pageId).getCreator();
			
			boolean isEdit = actionName.contains("Edit");
			boolean isApply = actionName.contains("apply");
			
			Page page = pageMan.getPage(pageId);
			if(userOrNull != null && isEdit && isApply) applyUserEditPermission(page, userOrNull);
			if(userOrNull != null && !isEdit && isApply) applyUserViewPermission(page, userOrNull);
			if(userOrNull != null && isEdit && !isApply) removeUserEditPermission(page, userOrNull);
			if(userOrNull != null && !isEdit && !isApply) removeUserViewPermission(page, userOrNull);
			if(userOrNull == null && isEdit && isApply) applyGroupEditPermission(page, groupOrUser);
			if(userOrNull == null && !isEdit && isApply) applyGroupViewPermission(page, groupOrUser);
			if(userOrNull == null && isEdit && !isApply) removeGroupEditPermission(page, groupOrUser);
			if(userOrNull == null && !isEdit && !isApply) removeGroupViewPermission(page, groupOrUser);
		}
		catch(Exception e) {
			log.error("Error in handling action {} for group or user {}: {}", actionName, groupOrUser, e.toString());
			log.debug(getStackTrace(e));			
		}
	}
	
	
	// find a user by its email
	private String findUserByEmail(String userName) {
		try {
			List<com.atlassian.user.User> results = userAcc.findUsersAsList(new EmailTermQuery(userName, EmailTermQuery.SUBSTRING_CONTAINS));
			if(results.size() == 1) return results.get(0).getName();
		}
		catch(Exception e) {}
		return null;
	}
	
	
	// apply a user edit permission on the given page
	private void applyUserEditPermission(Page page, ConfluenceUser user) throws Exception {
		saveAddContentPermission(ContentPermission.createUserPermission(ContentPermission.EDIT_PERMISSION, user), page);
	}
	
	
	// apply a user view permission on the given page
	private void applyUserViewPermission(Page page, ConfluenceUser user) throws Exception {
		saveAddContentPermission(ContentPermission.createUserPermission(ContentPermission.VIEW_PERMISSION, user), page);
	}

	
	// apply a group edit permission on the given page
	private void applyGroupEditPermission(Page page, String groupName) throws Exception {
		saveAddContentPermission(ContentPermission.createGroupPermission(ContentPermission.EDIT_PERMISSION, groupName), page);
	}
	
	
	// apply a group view permission on the given page
	private void applyGroupViewPermission(Page page, String groupName) throws Exception {
		saveAddContentPermission(ContentPermission.createGroupPermission(ContentPermission.VIEW_PERMISSION, groupName), page);
	}

	
	// add a content permission but check if it already exists before
	private void saveAddContentPermission(ContentPermission permission, Page page) {
		List<ContentPermissionSet> permissionSets = contentPermissionMan.getContentPermissionSets(page, permission.getType());
		ContentPermissionSet permissionSet = permissionSets.isEmpty() ? new ContentPermissionSet() : permissionSets.get(0);
		if(!permissionSet.contains(permission)) {
			contentPermissionMan.addContentPermission(permission, page);
		}
	}
	
	// remove a user edit permission on the given page
	private void removeUserEditPermission(Page page, ConfluenceUser user) throws Exception {
		saveRemoveContentPermission(ContentPermission.createUserPermission(ContentPermission.EDIT_PERMISSION, user), page);
	}
	
	
	// remove a user view permission on the given page
	private void removeUserViewPermission(Page page, ConfluenceUser user) throws Exception {
		saveRemoveContentPermission(ContentPermission.createUserPermission(ContentPermission.VIEW_PERMISSION, user), page);
	}

	
	// remove a group edit permission on the given page
	private void removeGroupEditPermission(Page page, String groupName) throws Exception {
		saveRemoveContentPermission(ContentPermission.createGroupPermission(ContentPermission.EDIT_PERMISSION, groupName), page);
	}
	
	
	// remove a group view permission on the given page
	private void removeGroupViewPermission(Page page, String groupName) throws Exception {
		saveRemoveContentPermission(ContentPermission.createGroupPermission(ContentPermission.VIEW_PERMISSION, groupName), page);
	}
	
	// remove a content permission but check if it exists before
	private void saveRemoveContentPermission(ContentPermission permission, Page page) {
		List<ContentPermissionSet> permissionSets = contentPermissionMan.getContentPermissionSets(page, permission.getType());
		ContentPermissionSet permissionSet = permissionSets.isEmpty() ? new ContentPermissionSet() : permissionSets.get(0);
		for(ContentPermission permissionInSet : permissionSet) {
			if(permissionInSet.equals(permission)) contentPermissionMan.removeContentPermission(permissionInSet);
		}
	}

	
	// get all users in the given groups matching the given query as JSON string
	public String getUsers(String query, String groupNames) {
	    JSONArray foundUsers = new JSONArray();

	    try {
	        // Find users matching the query
	        List<com.atlassian.user.User> results = userAcc.findUsersAsList(
	            new FullNameTermQuery(query, FullNameTermQuery.SUBSTRING_CONTAINS)
	        );
	        
	        // Split groupNames by comma (and optionally trim spaces)
	        String[] groupsArray = groupNames.split("\\s*,\\s*");
	        
	        for (com.atlassian.user.User result : results) {
	            // Fetch the user's group memberships once
	            List<String> userGroups = userAcc.getGroupNamesForUserName(result.getName());
	            
	            // Check whether the user is in any group specified in groupsArray
	            boolean isInAtLeastOneGroup = false;
	            for (String group : groupsArray) {
	                // Check group as-is or with "-sleep" suffix
	                if (userGroups.contains(group) || userGroups.contains(group + "-sleep")) {
	                    isInAtLeastOneGroup = true;
	                    break; // No need to check other groups once we have a match
	                }
	            }

	            // If user is in at least one of the specified groups, add them to JSON array
	            if (isInAtLeastOneGroup) {
	                JSONObject userJson = new JSONObject();
	                userJson.put("fullName", getCleanedFullName(result.getFullName()));
	                userJson.put("email", result.getEmail());
	                userJson.put("name", result.getName());
	                foundUsers.put(userJson);
	            }
	        }
	    } catch (Exception e) {
	        // Log/handle exception as appropriate
	    }

	    return foundUsers.toString();
	}
	
	
	// get the country code for the give user from active directory
	public String getUsersFromAD(String query) {
		JSONArray foundUsers = new JSONArray();

		try {
			// TODO: searching by name is not a good idea as it will fail as soon as somebody takes another name
			Map<String, String> dirAttributes = directoryMan.findDirectoryByName("Authentication with AD").getAttributes();			
			
			Properties props = new Properties();
		    props.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
		    props.put(Context.PROVIDER_URL, dirAttributes.get("ldap.url"));
		    props.put(Context.SECURITY_PRINCIPAL, dirAttributes.get("ldap.userdn"));
		    props.put(Context.SECURITY_CREDENTIALS, dirAttributes.get("ldap.password"));
	
		    InitialDirContext context = new InitialDirContext(props);
	
		    SearchControls ctrls = new SearchControls();
		    ctrls.setReturningAttributes(new String[] { "displayName", "sAMAccountName", "mail" });
		    ctrls.setSearchScope(SearchControls.SUBTREE_SCOPE);
	
		    NamingEnumeration<javax.naming.directory.SearchResult> answers = context.search("DC=itgr,DC=net", "(|(givenName=" + query + "*)(sn=" + query + "*))", ctrls);
		    
		    while(answers.hasMore()) {
			    javax.naming.directory.SearchResult result = answers.nextElement();
			    JSONObject user = new JSONObject();
			    user.put("fullName", getCleanedFullName(result.getAttributes().get("displayName").get().toString()));
				user.put("email",result.getAttributes().get("mail").get().toString());
				user.put("name", result.getAttributes().get("sAMAccountName").get().toString());
			    foundUsers.put(user);
		    }
		    
		    context.close();
		}
		catch(Exception e) {
		}
		
		return foundUsers.toString();
	}
		
	
	// get the name of the group to which an assignment is currently allowed
	public String getAllowedAssignmentGroups(Long pageId) {
		SafeJSONObject workflow = getApplicableWorkflow(pageId);
		String currentState = getCurrentStateName(pageId);
		for(int n = 0; n < workflow.getJSONArray("states").length(); n++) {
			SafeJSONObject state =  workflow.safeGetJSONArray("states").safeGetJSONObject(n);
			if(state.safeGetString("name", "name_not__defined").equals(currentState)) {
				return state.safeGetString("allowedAssignmentGroup", state.safeGetString("allowedAssignmentGroups", "allowedAssignmentGroup_not_found"));
			}
		}
		return "nothing_found";
	}
	
	
	// is the current user part of the agentGroup
	public boolean isCurrentUserAgent(Long pageId) {
		SafeJSONObject workflow = getApplicableWorkflow(pageId);
		return userAcc.getGroupNames(AuthenticatedUserThreadLocal.get()).contains(workflow.safeGetString("agentGroup", "agentGroup_not_found"));
	}
	
	
	// is the current user in a group to which an assigment is allowed
	public boolean isCurrentUserInAssignmentGroups(Long pageId) {
	    List<String> userGroups = userAcc.getGroupNames(AuthenticatedUserThreadLocal.get());
	    String allowedGroups = getAllowedAssignmentGroups(pageId);

	    if (allowedGroups == null || allowedGroups.trim().isEmpty()) {
	    	return false;
	    }

	    return Arrays.stream(allowedGroups.split(","))
             .map(String::trim)
             .anyMatch(userGroups::contains);
	}
	
	
	// get all group names of the current user as ;-seperated string
	public String getCurrentUsersGroupsAsString() {
		return String.join(";", userAcc.getGroupNames(AuthenticatedUserThreadLocal.get()));
	}
	
	
	// get the currently assigned user key from the content property
	private String getAssignedUser(Long pageId) {
		return propertyMan.getTextProperty(pageMan.getById(pageId), "smart-forms-assigned-user-key");
	}
	
	
	// get the name of the currently assigned user
	public String getAssignedUserName(Long pageId) {
		String currentAssignedUserKey =  getAssignedUser(pageId);
		if(currentAssignedUserKey != null) {
			ConfluenceUser currentAssignedUser = userAcc.getUserByKey(new UserKey(currentAssignedUserKey));
			if(currentAssignedUser != null) {
				return currentAssignedUser.getFullName();
			}
		}
		
		return  i18n.getText("de.edrup.confluence.plugins.smart-forms.form.unAssigned");
	}
	
	
	// assign a user with the given name to the form instance
	public boolean assignUser(Long pageId, String userName) {
		
		Page p = pageMan.getPage(pageId);
		String currentAssignedUserKey = getAssignedUser(pageId);
				
		// assign new user
		if((userName != null) && !"".equals(userName))
		{
			// set new assignment
			ConfluenceUser user = userAcc.getUserByName(userName);
			propertyMan.setTextProperty(p, "smart-forms-assigned-user-key", user.getKey().getStringValue());
			
			// create new page version and send email about assignment
			if(!user.getKey().getStringValue().equals(currentAssignedUserKey)) {
				createNewPageVersion(pageId, i18n.getText("de.edrup.confluence.plugins.smart-forms.versionComment.assign").replace("$userName", userName));
				sendAssignedEmail(user.getKey().getStringValue(), pageId);
			}
			
			// remove old watch
			if(currentAssignedUserKey != null) {
				ConfluenceUser currentAssignedUser = userAcc.getUserByKey(new UserKey(currentAssignedUserKey));
				if(currentAssignedUser != null) {
					notificationMan.removeContentNotification(currentAssignedUser, p);
				}
			}
			
			// add watch
			if(!notificationMan.isWatchingContent(user, p)) {
				notificationMan.addContentNotification(user, p);
			}
		}
		
		// remove assignment
		else {
			// create new page version
			propertyMan.removeProperty(p, "smart-forms-assigned-user-key");
			createNewPageVersion(pageId, i18n.getText("de.edrup.confluence.plugins.smart-forms.versionComment.assign").replace("$userName", i18n.getText("de.edrup.confluence.plugins.smart-forms.form.unAssigned")));
			
			// remove old watch
			if(currentAssignedUserKey != null) {
				ConfluenceUser currentAssignedUser = userAcc.getUserByKey(new UserKey(currentAssignedUserKey));
				if(currentAssignedUser != null) {
					notificationMan.removeContentNotification(currentAssignedUser, p);
				}
			}
		}
		
		return true;
	}
	
	
	// get all the data from the form + current state and assigned user key
	public JSONObject getAllFormData(Long pageId) {
		JSONObject allData = new JSONObject(getCurrentFormData(pageId));
		if(isWorkflowDefined(pageId)) {
			allData.put("workflowState", getCurrentStateName(pageId));
			String assignedUserKey = getAssignedUser(pageId);
			allData.put("assignedUserKey", (assignedUserKey != null) ? assignedUserKey : "unassigned");
		}
		allData.put("creatorEmailEnding", getCountryCode(pageMan.getPage(pageId).getCreator().getName()));
		
		return allData;
	}
	
	
	// get all form data + current state and assigned user name as Confluence table
	private String getAllFormDataAsConfluenceTable(Long pageId) {
		String table = "<table class=\"smart-forms-table wrapped\"><tbody><tr><th>Name</th><th>Value</th></tr>";
		if(isWorkflowDefined(pageId)) {
			table += "<tr><td>workflowState</td><td>" + getCurrentStateName(pageId) + "</td></tr>";
			table += "<tr><td>assignedUser</td><td>" + getAssignedUserName(pageId) + "</td></tr>";
		}
		JSONObject formData = new JSONObject(getCurrentFormData(pageId));
		Iterator<String> keys = formData.keys();
		while(keys.hasNext()) {
			String key = keys.next();
			String value = formData.getString(key);
			table += "<tr><td>" + key + "</td><td>" + value + "</td></tr>";
		}
		table += "</tbody></table>";
		
		return table;
	}
	
	
	// create a new page version showing the current form data + current state and assigned user name
	private void createNewPageVersion(Long pageId, String versionComment) {
		String body = pageMan.getPage(pageId).getBodyAsString();
		log.debug("Page content {} old: {}", pageId, body);
		
		body = body.replaceAll("(?s)<table class=\"smart-forms-table.*<\\/table>", "");
		body = body.concat(getAllFormDataAsConfluenceTable(pageId));
		log.debug("Page content {} new: {}", pageId, body);
		
		pageMan.saveNewVersion(pageMan.getPage(pageId), new SmartFormsPageModification(body, versionComment), buildSaveContext());
	}
	
	
	// render the requested overview result
	public String getOverview(Long templatePageId, String macroId, String query, int start, int limit, Long embeddingPageId) {

		JSONObject result = new JSONObject();
		
		try {
			SmartFormsPageResult pageResult = query.isEmpty() ? new SmartFormsPageResult() : getPagesMatchingQuery(query, start, limit);
			
			// fix Confluence 8.x issues with a commonly used pattern
			String regex = "\\$(\\w+)\\.titleWithLinkWithHtml";
	        String replacement = "#set(\\$titleWithLinkWithHtml = \\$$1.titleWithLinkWithHtml)\\$titleWithLinkWithHtml";
	        Pattern pattern = Pattern.compile(regex);
	        Matcher matcher = pattern.matcher(getBodyContentOfMacro(templatePageId, macroId));
	        String template = matcher.replaceAll(replacement);

			Map<String, Object> context = MacroUtils.defaultVelocityContext();
			context.put("results", getInstanceDataOfList(pageResult.getResults()));
			context.put("embeddingPageId", embeddingPageId);
			context.put("currentUserKey", AuthenticatedUserThreadLocal.get().getKey().getStringValue());
	
			result.put("renderedTemplate",  VelocityUtils.getRenderedContent((CharSequence) template, context).replace("\u00a0",""));
			result.put("totalSize", pageResult.getTotalSize());			
		}
		catch(Exception e) {
			result.put("renderedTemplate", getStackTrace(e));
			result.put("totalSize", 0);
		}
		
		return result.toString();
	}
	
	
	// get the overview results as JSON
	public JSONArray getOverviewJSON(String query, int start, int limit) {
		try {
			SmartFormsPageResult pageResult = query.isEmpty() ? new SmartFormsPageResult() : getPagesMatchingQuery(query, start, limit);
			List<JSONObject> results = getInstanceDataOfList(pageResult.getResults());
			return new JSONArray(results);
		}
		catch(Exception e) {
			return new JSONArray();
		}
	}
	
	
	// get all form data plus additional data for a list of page Ids
	private ArrayList<JSONObject> getInstanceDataOfList(List<Long> pageIds) {
		ArrayList<JSONObject> results = new ArrayList<JSONObject>();
		for(Long pageId : pageIds) {
			results.add(getInstanceData(pageId));
		}
		return results;
	}
	
	
	// get all form data plus additional data for one page Id
	private JSONObject getInstanceData(Long pageId) {
		JSONObject allFormData = new JSONObject();
		try {
			allFormData = getAllFormData(pageId);
			Page p = pageMan.getPage(pageId);
			allFormData.put("stateColor", getCurrentStateColor(pageId));
			allFormData.put("stateTextColor", getCurrentStateTextColor(pageId));
			allFormData.put("creationDate", getInISO8601UTC(p.getCreationDate()));
			allFormData.put("lastModificationDate", getInISO8601UTC(p.getLastModificationDate()));
			allFormData.put("lastModifierName", p.getLastModifier().getFullName());
			allFormData.put("creatorFullName", p.getCreator().getFullName());
			allFormData.put("creatorEmail", p.getCreator().getEmail());
			allFormData.put("titleWithLinkWithHtml", getTitleWithLink(p));
			allFormData.put("title", p.getTitle());
			allFormData.put("link", getBaseUrl() + p.getUrlPath());
			allFormData.put("overdue", isOverdue(pageId));
			allFormData.put("daysTillDueDate", daysTillDueDate(pageId));
			allFormData.put("assignedUserName", getAssignedUserName(pageId));
			allFormData.put("pageID", pageId);
			allFormData.put("key", p.getTitle());
		}
		catch(Exception e) {
			log.error(e.toString());
		}
		return allFormData;
	}
	
	
	// get all pages matching the given query
	public SmartFormsPageResult getPagesMatchingQuery(String query, int start, int limit) {
		
		SmartFormsPageResult result = new SmartFormsPageResult();
		
		for(int batchStart = start; batchStart < start + limit; batchStart = batchStart + BATCH_LIMIT) {
			@SuppressWarnings("rawtypes")
			SearchPageResponse<SearchResult> searchResult = searchService.search(query, SearchOptions.builder().excerptStrategy(SearchOptions.Excerpt.NONE).build(), new SimplePageRequest(batchStart, (limit < BATCH_LIMIT ? limit : BATCH_LIMIT)), Expansion.combine(""));
			result.addAll(searchResult.getResults().stream().map(x -> ((com.atlassian.confluence.api.model.content.Content) x.getEntity()).getId().asLong()).collect(Collectors.toList()));
			result.setTotalSize(searchResult.totalSize());
		}
		
		return result;
	}
	
	
	// get the given date in ISO 8601 format
	private String getInISO8601UTC(Date d) {
		  DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
		  df.setTimeZone(TimeZone.getTimeZone("UTC"));
		  return df.format(d);
	}
	
	
	// get a named link to the given page
	private String getTitleWithLink(Page p) {
		return String.format("<a href='%s'>%s</a>", getBaseUrl() + p.getUrlPath(), HtmlUtil.htmlEncode(p.getTitle()));
	}
	
	
	// get the base url
	public String getBaseUrl() {
		return  settingsMan.getGlobalSettings().getBaseUrl();
	}
	
	
	// send out an assignment message
	private void sendAssignedEmail(String userKey, Long pageId) {
		ConfluenceUser user = userAcc.getUserByKey(new UserKey(userKey));
		SafeJSONObject workflow = getApplicableWorkflow(pageId);
		sendEmail(user, workflow.safeGetString("mailFromName", "No from name defined"),
			fillTitlePlaceholders(workflow.safeGetString("assignmentMailTitle", "no mail title defined in workflow"), pageId, null),
			fillMessagePlaceholders(workflow.safeGetString("assignmentMailMessage", "no mail message defined in workflow"), pageId, null));
	}
	
	
	// notify a group of users
	// in case linkedPageId is not null the data is taken from the linked page
	private void notifyGroup(Long pageId, Long linkedPageId, String groupName, String title, String message) {
		try {
			Long usePageId = linkedPageId == null ? pageId: linkedPageId;
			ConfluenceUser currentUser = AuthenticatedUserThreadLocal.get();
			Iterator<String> membersIt = null;
			if(groupName.equals("creator-group")) {
				ArrayList<String> creatorList = new ArrayList<String>();
				creatorList.add(pageMan.getPage(usePageId).getCreator().getName());
				membersIt = creatorList.iterator();
			}
			else if(groupName.equals("assigned-group")) {
				ArrayList<String> assignedList = new ArrayList<String>();
				String currentAssignedUserKey =  getAssignedUser(usePageId);
				if(currentAssignedUserKey != null) {
					assignedList.add(userAcc.getUserByKey(new UserKey(currentAssignedUserKey)).getName());
				}
				membersIt = assignedList.iterator();
			}
			else if(groupName.equals("contributor-group")) {
				membersIt = getContributors(usePageId, false).iterator();
			}
			else if(groupName.equals("contributor-not-creator-group")) {
				membersIt = getContributors(usePageId, true).iterator();
			}
			else {
				membersIt = groupMan.getMemberNames(groupMan.getGroup(groupName)).iterator();
			}
			SafeJSONObject workflow = getApplicableWorkflow(usePageId);
			while(membersIt.hasNext()) {
				try {
					ConfluenceUser user = userAcc.getUserByName(membersIt.next());
					if(!user.equals(currentUser)) {
						sendEmail(user,  workflow.safeGetString("mailFromName", "No from name defined"), fillTitlePlaceholders(title, pageId, linkedPageId), fillMessagePlaceholders(message, pageId, linkedPageId));
					}
				}
				catch(Exception e) {
					log.error(e.toString());
					log.debug(getStackTrace(e));
				}
			}
		}
		catch(Exception e) {
			log.error(e.toString());
			log.debug(getStackTrace(e));
		}
	}
	

	// notify the user(s) defined in the given field name
	// in case linkedPageId is not null the the field name is taken from the linked page
	private void notifyUserFromField(Long pageId, Long linkedPageId, String fieldName, String title, String message) {
		try {
			Long usePageId = linkedPageId == null ? pageId: linkedPageId;
			SafeJSONObject workflow = getApplicableWorkflow(usePageId);
			List<String> userNames = Arrays.asList(getAllFormData(usePageId).getString(fieldName).replace("select2:", "").replace("multiple:", "").split(","));
			for(String userName : userNames) {
				if(!userName.isEmpty()) {
					String nameOrEmail = userName.endsWith(")") ? userName.replaceFirst(".*?\\(", "").replace(")", "") : userName;
					String fullName = userName.replaceFirst(" *\\(.*?\\)", "");
					if(nameOrEmail.contains("@")) {
						sendEmail(fullName, nameOrEmail, workflow.safeGetString("mailFromName", "No from name defined"), fillTitlePlaceholders(title, pageId, linkedPageId), fillMessagePlaceholders(message, pageId, linkedPageId));
					}
					else {
						ConfluenceUser user = userAcc.getUserByName(nameOrEmail);
						sendEmail(user, workflow.safeGetString("mailFromName", "No from name defined"), fillTitlePlaceholders(title, pageId, linkedPageId), fillMessagePlaceholders(message, pageId, linkedPageId));
					}
				}
			}
		}
		catch(Exception e) {
			log.error(e.toString());
			log.debug(getStackTrace(e));
		}
	}
	
	
	// notify the given email address
	private void notifyEmail(Long pageId, String email, String title, String message) {
		try {
			SafeJSONObject workflow = getApplicableWorkflow(pageId);
			sendEmail("user", email, workflow.safeGetString("mailFromName", "No from name defined"), fillTitlePlaceholders(title, pageId, null), fillMessagePlaceholders(message, pageId, null));
		}
		catch(Exception e) {
			log.error(e.toString());
			log.debug(getStackTrace(e));
		}
	}
	
	
	// get a list of contributors
	private List<String> getContributors(Long pageId, boolean excludeCreator) {
		ArrayList<String> contributors = new ArrayList<String>();
		Page p = pageMan.getPage(pageId);
		for(int n = 1; n < p.getVersion(); n++) {
			try {
				String userName = pageMan.getPageByVersion(p, n).getLastModifier().getName();
				if(!contributors.contains(userName)) {
					contributors.add(pageMan.getPageByVersion(p, n).getLastModifier().getName());
				}
			}
			catch(Exception e) {
			}
		}
		if(excludeCreator) {
			contributors.remove(p.getCreator().getName());
		}
		return contributors;
	}
	
	
	// send an email
	private void sendEmail(ConfluenceUser user, String fromName, String title, String message) {
		MailServerManager mailServerMan = MailFactory.getServerManager();
		if((user.getEmail().length() > 0) && mailServerMan.isDefaultSMTPMailServerDefined() && mailServerMan.getDefaultSMTPMailServer() instanceof JmxSMTPMailServer) {
			message = message.replace("$recipient", user.getFullName());
			ConfluenceMailQueueItem mqi = new ConfluenceMailQueueItem(user.getEmail(), title, message, MIME_TYPE_HTML);
			mqi.setFromName(((JmxSMTPMailServer) mailServerMan.getDefaultSMTPMailServer()).getFromName().replace("${fullname}",fromName));
			mqtm.addTask("mail", () -> mqi.send());
			log.debug("Sending message to {} with title {} and content {}", user.getEmail(), title, message);
		}
	}
	
	
	// send an email
	private void sendEmail(String fullName, String email, String fromName, String title, String message) {
		MailServerManager mailServerMan = MailFactory.getServerManager();
		if(mailServerMan.isDefaultSMTPMailServerDefined() && mailServerMan.getDefaultSMTPMailServer() instanceof JmxSMTPMailServer) {
			message = message.replace("$recipient", fullName);
			ConfluenceMailQueueItem mqi = new ConfluenceMailQueueItem(email, title, message, MIME_TYPE_HTML);
			mqi.setFromName(((JmxSMTPMailServer) mailServerMan.getDefaultSMTPMailServer()).getFromName().replace("${fullname}",fromName));
			mqtm.addTask("mail", () -> mqi.send());
			log.debug("Sending message to {} with title {} and content {}", email, title, message);
		}
	}
	
	
	// fill message placeholders
	private String fillMessagePlaceholders(String message, Long pageId, Long linkedPageId) {
		ConfluenceUser currentUser = AuthenticatedUserThreadLocal.get();
		JSONObject formData = getInstanceData(pageId);
		message = message.replace("$titleWithLink", getTitleWithLink(pageMan.getPage(pageId)))
				.replace("$currentUser", currentUser != null ? currentUser.getFullName() : "unknown")
				.replace("$content", pageMan.getPage(pageId).getBodyAsString())
				.replace("$formData", getAllFormDataAsConfluenceTable(pageId));

		// replace field-names as suggested by ChatGPT
		Pattern variablePattern = Pattern.compile("\\$data_(\\w+)");
		Matcher matcher = variablePattern.matcher(message);
		StringBuffer result = new StringBuffer();

		while (matcher.find()) {
			String fieldName = matcher.group(1);
			String replacement = formData.optString(fieldName, "field not found");
			matcher.appendReplacement(result, Matcher.quoteReplacement(replacement));
		}
		matcher.appendTail(result);
		message = result.toString();

		if (linkedPageId != null) {
			message = message.replace("$linkedTitleWithLink", getTitleWithLink(pageMan.getPage(linkedPageId)))
				.replace("$linkedContent", pageMan.getPage(linkedPageId).getBodyAsString())
				.replace("$linkedFormData", getAllFormDataAsConfluenceTable(linkedPageId));
		}
		return message;
	}
	
	
	// fill title placeholders
	private String fillTitlePlaceholders(String title, Long pageId, Long linkedPageId) {
		JSONObject formData = getInstanceData(pageId);
		
		title = title.replace("$title", pageMan.getPage(pageId).getTitle());
		
		// replace field-names as suggested by ChatGPT
		Pattern variablePattern = Pattern.compile("\\$data_(\\w+)");
		Matcher matcher = variablePattern.matcher(title);
		StringBuffer result = new StringBuffer();

		while (matcher.find()) {
			String fieldName = matcher.group(1);
			String replacement = formData.optString(fieldName, "field not found");
			matcher.appendReplacement(result, Matcher.quoteReplacement(replacement));
		}
		matcher.appendTail(result);
		title = result.toString();
		
		if(linkedPageId != null) {
			title = title.replace("$linkedTitle",  pageMan.getPage(linkedPageId).getTitle());
		}
		return title;
	}

	
	// was the last change made an admin or space admin
	public boolean lastChangeMadeBySpaceOrConfAdmin(Long pageId) {
		try {
			Page p = pageMan.getPage(pageId);
			return spaceMan.getSpaceAdmins(p.getSpace()).contains(p.getLastModifier()) | permissionMan.isConfluenceAdministrator(p.getLastModifier());
		}
		catch(Exception e) {
			return false;
		}
	}


	// get the detailed stack trace
	public String getStackTrace(final Throwable throwable) {
	     final StringWriter sw = new StringWriter();
	     final PrintWriter pw = new PrintWriter(sw, true);
	     throwable.printStackTrace(pw);
	     return sw.getBuffer().toString();
	}
	
	
	// check if a due date field is there
	public boolean hasDueDateField(Long pageId) {
		SafeJSONObject currentFormData = new SafeJSONObject(getCurrentFormData(pageId));
		SafeJSONObject workflow = getApplicableWorkflow(pageId);
		if(workflow != null) {
			return currentFormData.has(workflow.safeGetString("dueDateFieldName", "autoDueDate"));
		}
		return false;
	}
	
	
	// save the current due date
	private void setDueDate(Long pageId, String dateString) {
		SafeJSONObject workflow = getApplicableWorkflow(pageId);
		setFieldValue(pageId, workflow.safeGetString("dueDateFieldName", "autoDueDate"), dateString, true);
	}
	
	
	// save the current due date
	public String getDueDate(Long pageId) {
		if(hasDueDateField(pageId)) {
			SafeJSONObject currentFormData = new SafeJSONObject(getCurrentFormData(pageId));
			SafeJSONObject workflow = getApplicableWorkflow(pageId);
			return currentFormData.safeGetString(workflow.safeGetString("dueDateFieldName", "autoDueDate"), "");
		}
		else {
			return "";
		}
	}
	
	
	// get the sort field values
	public HashMap<String, String> getSortValues(Long pageId) {
		HashMap<String, String> sortValues = new HashMap<String, String>();
		
		SafeJSONObject currentFormData = new SafeJSONObject(getCurrentFormData(pageId));
		SafeJSONObject workflow = getApplicableWorkflow(pageId);
		
		if(workflow != null) {
			JSONArray sortFields = workflow.safeGetJSONArray("sortFields");
			for(int n = 0; n < sortFields.length(); n++) {
				try {
					JSONObject sortField = sortFields.getJSONObject(n);
					sortValues.put(sortField.getString("sortName"), currentFormData.safeGetString(sortField.getString("fieldName"), null));
				}
				catch(Exception e) {
				}
			}
		}
		
		return sortValues;
	}
	
	
	// set the current due date in hours from now or the last due date
	private void setDueDate(Long pageId, boolean fromLastDueDate, int workingHoursOffset) {
		
	    int MAX_LOOPS = 10;
	    
		Date currentDueDate = ((getDueDate(pageId) == null) || !fromLastDueDate) ? new Date() : convertString2Date(getDueDate(pageId));
		Calendar cal = Calendar.getInstance();
		cal.setTimeZone(TimeZone.getTimeZone("UTC"));
	    cal.setTime(currentDueDate);
	    
	    // get working days and time
	    SafeJSONObject workflow = getApplicableWorkflow(pageId);
	    String workingDays = workflow.safeGetString("workingDays", "23456");
	    int workingDayStartHour = workflow.safeGetInt("workingDayStartHour", 8);
	    int workingDayEndHour = workflow.safeGetInt("workingDayEndHour", 16);
	    
	    // days and hours to shift
	    int wholeDays = workingHoursOffset / (workingDayEndHour - workingDayStartHour);
	    int missingHours = workingHoursOffset - (wholeDays * (workingDayEndHour - workingDayStartHour));
	    int loops = 0;
	    
	    // ensure to start with a working day
	    while(!workingDays.contains(Integer.toString(cal.get(Calendar.DAY_OF_WEEK))) && (loops < MAX_LOOPS)) {
	    	 cal.add(Calendar.HOUR_OF_DAY, 24);
	    	 cal.set(Calendar.HOUR_OF_DAY, workingDayStartHour);
	    	 loops++;
	    }
	    
	    // ensure to start in the working time
	    if(cal.get(Calendar.HOUR_OF_DAY) < workingDayStartHour) {
	    	cal.set(Calendar.HOUR_OF_DAY, workingDayStartHour);
	    	cal.set(Calendar.MINUTE, 0);
	    	cal.set(Calendar.SECOND, 0);
	    }
	    else if(cal.get(Calendar.HOUR_OF_DAY) > workingDayEndHour) {
	    	cal.add(Calendar.HOUR_OF_DAY, 24);
	    	cal.set(Calendar.HOUR_OF_DAY, workingDayStartHour);
	 	    while(!workingDays.contains(Integer.toString(cal.get(Calendar.DAY_OF_WEEK))) && (loops < MAX_LOOPS)) {
	 	    	 cal.add(Calendar.HOUR_OF_DAY, 24);
	 	    	 cal.set(Calendar.HOUR_OF_DAY, workingDayStartHour);
	 	    	 loops++;
	 	    }
	    }
	    
	    // add missing hours and ensure that the end date is within working hours
	    cal.add(Calendar.HOUR_OF_DAY, missingHours);
	    if((cal.get(Calendar.HOUR_OF_DAY) > workingDayEndHour) || (cal.get(Calendar.HOUR_OF_DAY) < workingDayStartHour)) {
	    	cal.add(Calendar.HOUR_OF_DAY, 24 - (workingDayEndHour - workingDayStartHour));
	    }
	    
	    // increase the days
	    for(int n = 0; n < wholeDays; n++) {
	    	 cal.add(Calendar.HOUR_OF_DAY, 24);
	    	 // and ensure that it is an increase in working days
	    	 while(!workingDays.contains(Integer.toString(cal.get(Calendar.DAY_OF_WEEK))) && (loops < MAX_LOOPS)) {
		    	 cal.add(Calendar.HOUR_OF_DAY, 24);
		    	 loops++;
		    }
	    }
	    
	    setDueDate(pageId, getInISO8601UTC(cal.getTime()));
	}
	
	
	// convert string to a date
	public Date convertString2Date(String dateString) {
		String allowedFormats[] = { "yyyy-MM-dd'T'HH:mm:ss'Z'", "yyyy-MM-dd HH:mm", "yyyy-MM-dd"};
		for(String allowedFormat : allowedFormats) {
			try {
				DateFormat df = new SimpleDateFormat(allowedFormat);
				df.setTimeZone(TimeZone.getTimeZone("UTC"));
				return df.parse(dateString);
			}
			catch(Exception e) {
			}
		}
		return new Date();
	}
	
	
	//  check if the due date of the given page is before the current date
	private boolean isOverdue(Long pageId) {
		if(hasDueDateField(pageId)) {
			Date dueDate = convertString2Date(getDueDate(pageId));
			return dueDate.before(new Date());
		}
		return false;
	}
	
	
	//  calculate the difference in days between now and the due date
	private long daysTillDueDate(Long pageId) {
		if(hasDueDateField(pageId)) {
			Date dueDate = convertString2Date(getDueDate(pageId));
			return TimeUnit.DAYS.convert(dueDate.getTime() - (new Date()).getTime(), TimeUnit.MILLISECONDS);
		}
		return 0L;
	}
	
	
	// get te country code for the given user
	private String getCountryCode(String userName) {
		String countryCode = countryCache.get(userName);
		if(countryCode == null) {
			countryCode = getAttributeFromADForUser(userName, "c").toLowerCase();
			if(!countryCode.equals("??")) {
				countryCache.put(userName, countryCode);
			}
		}
		return countryCode;
	}
	
	
	// get a single attribute for the given user from active directory
	public String getAttributeFromADForUser(String userName, String attributeName) {
		return getAttributeFromAD("sAMAccountName", userName, attributeName);
	}
	
		
	// get a single attribute for the given user from active directory
	public String getAttributeFromAD(String queryField, String query, String attributeName) {
		try {
			// TODO: searching by name is not a good idea as it will fail as soon as somebody takes another name
			Map<String, String> dirAttributes = directoryMan.findDirectoryByName("Authentication with AD").getAttributes();			
			
			Properties props = new Properties();
		    props.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.ldap.LdapCtxFactory");
		    props.put(Context.PROVIDER_URL, dirAttributes.get("ldap.url"));
		    props.put(Context.SECURITY_PRINCIPAL, dirAttributes.get("ldap.userdn"));
		    props.put(Context.SECURITY_CREDENTIALS, dirAttributes.get("ldap.password"));
	
		    InitialDirContext context = new InitialDirContext(props);
	
		    SearchControls ctrls = new SearchControls();
		    ctrls.setReturningAttributes(new String[] { attributeName });
		    ctrls.setSearchScope(SearchControls.SUBTREE_SCOPE);
	
		    NamingEnumeration<javax.naming.directory.SearchResult> answers = context.search("DC=itgr,DC=net", "(&(objectCategory=Person)(" + queryField + "=" + query + "))", ctrls);
		    javax.naming.directory.SearchResult result = answers.nextElement();
		    String attribute = result.getAttributes().get(attributeName).get().toString();
		    
		    context.close();
		    
		    return attribute;
		}
		catch(Exception e) {
			return "??";
		}
	}
	
	
	// export the given query to an Excel file
	public String createExcelFile(String query, String columns) {
		
		try {			
			JSONArray overviewData = getOverviewJSON(query, 0, 10000);
			List<String> keys = columns.isBlank() ? getAllKeys(overviewData) : Arrays.asList(columns.split(" *, *"));
	        
			DownloadResourceWriter downloadResourceWriter = downloadResourceMan.getResourceWriter(
				AuthenticatedUserThreadLocal.getUsername(), "smartforms_export_", ".xlsx");
			OutputStream os = downloadResourceWriter.getStreamForWriting();
			
			Workbook wb = new Workbook(os, "SmartFormsExportWB", "1.0");
			Worksheet ws = wb.newWorksheet("Export");
			for(int c = 0; c < keys.size(); c++) {
				ws.value(0, c, keys.get(c));
			}
			
			for(int r = 0; r < overviewData.length(); r++) {
				for(int c = 0; c < keys.size(); c++) {
					if(overviewData.getJSONObject(r).has(keys.get(c)))  {
						Object o = overviewData.getJSONObject(r).get(keys.get(c));
						if(o instanceof String) {
							
							// is String a date?
							Date d = tryParse((String) o);
							if(d != null) {
								ws.value(r + 1, c, d);
								ws.style(r + 1, c).format("yyyy-MM-dd").set();
							}
							
							// String is not a date
							else {
								// is String a number?
								if(((String) o).matches("[+-]?[0-9]*[.,]?[0-9]+")) {
									ws.value(r + 1, c, Double.parseDouble(((String) o).replace(",", ".")));
								}
								// String is not a number
								else {
									ws.value(r + 1, c, ((String) o).replace("multiple:", "").replace("select2:", ""));
								}
							}
						}
						else if(o instanceof Number) {
							ws.value(r + 1, c, (Number) o);
						}
						else if(o instanceof Boolean) {
							ws.value(r + 1, c, (Boolean) o);
						}
						else {
							ws.value(r + 1, c, "unknown type");
						}
					}
					else {
						ws.value(r + 1, c, "");
					}
				}
			}
			
			wb.finish();
			os.flush();
			os.close();
			return getBaseUrl().concat(downloadResourceWriter.getResourcePath());
		}
		catch(Exception e) {
			log.error(getStackTrace(e));
			return "";
		}
	}
	
	
	// get all keys of the JSONArray
	private List<String> getAllKeys(JSONArray results) {
		List<String> keys = new ArrayList<String>();
		for(int n = 0; n < results.length(); n++) {
			JSONObject result = results.getJSONObject(n);
			Iterator<String> keyIt = result.keys();
			while(keyIt.hasNext()) {
				String key = keyIt.next();
				if(!keys.contains(key)) {
					keys.add(key);
				}
			}
		}
		return keys;
	}
	
	
	// return a date if string is a date
	private Date tryParse(String d) {
    	try {
    		if(d.matches("\\d{4}-\\d{2}-\\d{2}.*")) {
    			return DatatypeConverter.parseDateTime(d).getTime();
    		}
    		else {
    			return null;
    		}
    	}
    	catch(Exception e) {
    		return null;
    	}
    }
	
	
	// check whether the current user is allowed to edit the form
	public boolean formEditForCurrentUserAllowed(Long pageId) {
		String currentStateAllowedEditGroups = getCurrentStateAllowedEditGroups(pageId);
		if(currentStateAllowedEditGroups.isEmpty()) {
			return true;
		}
		else {
			ArrayList<String> groupNames = new ArrayList<String>();
			ConfluenceUser currentUser = AuthenticatedUserThreadLocal.get();
			groupNames.addAll(userAcc.getGroupNames(currentUser));
			if(pageMan.getPage(pageId).getCreator() == currentUser) {
				groupNames.add("creator-group");
			}
			if(currentUser.getKey().getStringValue() == getAssignedUser(pageId)) {
				groupNames.add("assigned-group");
			}
			return !Collections.disjoint(groupNames, Arrays.asList(currentStateAllowedEditGroups.split(" *, *")));
		}
	}
	
	
	// get the groups which are allowed to edit in the current state
	public String getCurrentStateAllowedEditGroups(Long pageId) {
		String currentState = getCurrentStateName(pageId);
		SafeJSONObject workflow = getApplicableWorkflow(pageId);
		for(int n = 0; n < workflow.getJSONArray("states").length(); n++) {
			SafeJSONObject state =  workflow.safeGetJSONArray("states").safeGetJSONObject(n);
			if(state.safeGetString("name","name_not_found").equals(currentState)) {
				return state.safeGetString("allowedEditGroups", "");
			}
		}
		return "";
	}
	
	
	// clone a form page
	public Long clonePage(Long clonePageId, boolean includeAttachments) {
		Page clonePage = pageMan.getPage(clonePageId);	
		Long newPageId = createPage(getApplicableFormPageId(clonePageId), getApplicableWorkflowPageId(clonePageId), clonePage.getParent().getId(), clonePage.getTitle().replaceFirst("-.*", ""));
		
		JSONObject formData = new SafeJSONObject(getCurrentFormData(clonePageId));
		
		try {
			JSONObject workflow = getApplicableWorkflow(clonePageId);
			if(workflow.has("cloning")) {
				JSONArray clearFields = workflow.getJSONObject("cloning").getJSONArray("clearFields");
				for(int n = 0; n < clearFields.length(); n++) {
					formData.put(clearFields.getString(n), "");
				}
			}
		}
		catch(Exception e) {
			log.error("Error clearing fields after cloning page id {}: {}", clonePageId, e.toString());
		}
		
		setFormData(newPageId, formData.toString());
		
		try {
			if(includeAttachments) {
				Page newPage = pageMan.getPage(newPageId);
				attachmentMan.copyAttachments(clonePage, newPage);
				cloneAttachmentLabels(clonePage, newPage);
			}
		}
		catch(Exception e) {
			log.error("Error copying attachments from page id {}: {}", clonePageId, e.toString());
		}
		
		return newPageId;
	}
	
	
	
	// clone the attachments' labels
	private void cloneAttachmentLabels(Page clonePage, Page newPage) {
		List<Attachment> attachments = clonePage.getAttachments();
		for(Attachment attachment : attachments) {
			List<Label> labels = attachment.getLabels();
			Attachment targetAttachment = newPage.getAttachmentNamed(attachment.getFileName());
			for(Label label : labels) {
				labelMan.addLabel(targetAttachment, label);
			}
		}
	}
	
	
	// clone a page multiple times
	public void multiClonePage(Long clonePageId, int count, boolean includeAttachments) {
		String currentUserKey = AuthenticatedUserThreadLocal.get().getKey().getStringValue();
		CompletableFuture.runAsync(() -> multiClonePageInTransaction(currentUserKey, clonePageId, count, includeAttachments));
	}
	
	
	// clone a page multiple times in a transaction
	private void multiClonePageInTransaction(String currentUserKey, Long clonePageId, int count, boolean includeAttachments) {
		transactionTemplate.execute(() -> {
			AuthenticatedUserThreadLocal.set(userAcc.getUserByKey(new UserKey(currentUserKey)));
			for(int n = 0; n < count; n++) {
				clonePage(clonePageId, includeAttachments);
			}
			return true;
		});
	}
	
	
	// check if cloning is allowed
	public boolean isCloningAllowed(Long pageId) {
		SafeJSONObject workflow = getApplicableWorkflow(pageId);
		return workflow.has("cloning");
	}
	
	
	// get the pageId of the linked instance
	public Long getLinkedInstance(Long pageId, String fieldName) {
		try {
			JSONObject formData = new SafeJSONObject(getCurrentFormData(pageId));
			if(formData.has(fieldName)) {
				String linkedPageId = formData.getString(fieldName);
				return linkedPageId.isEmpty() ? null : Long.parseLong(linkedPageId);
			}
		}
		catch(Exception e) {
		}
		return null;
	}
	
	
	// re-index a page now
	public void reIndexPage(Long pageId) {
		confluenceIndexer.synchronous().reIndex(pageMan.getPage(pageId));
	}
	
	
	// check if current user has a view permission on the give page
	public boolean hasViewPermission(Long pageId) {
		return permissionMan.hasPermission(AuthenticatedUserThreadLocal.get(), Permission.VIEW, pageMan.getPage(pageId));
	}
	
	
	// check whether the provided version matches the current version of the page
	public boolean checkFormVersion(Long pageId, Integer version) {
		Integer currentVersion = pageMan.getPage(pageId).getVersion();
		boolean check = version.equals(currentVersion);
		if(!check) {
			log.warn("Form version check failed: version of the form is {}, JS version is {}", currentVersion, version);
		}
		return check;
	}
	
	
	public Integer getFormVersion(Long pageId) {
		return pageMan.getPage(pageId).getVersion();
	}
	
	
	// switch user to the user defined in the field
	private void changeCreator(Long pageId, String fieldName) {
		try {
			String userName = getAllFormData(pageId).getString(fieldName);
			userName = userName.endsWith(")") ? userName.replaceFirst(".*?\\(", "").replace(")", "") : userName;
			ConfluenceUser user = userAcc.getUserByName(userName);
			if(user != null) {
				pageMan.saveNewVersion(pageMan.getPage(pageId),
					new SmartFormsCreatorModification(user, i18n.getText("de.edrup.confluence.plugins.smart-forms.versionComment.creatorChange")),
					buildSaveContext());
			}
			
		}
		catch(Exception e) {
			log.error(e.toString());
			log.debug(getStackTrace(e));
		}
	}
	
	
	// build a suitable SaveContext
	private SaveContext buildSaveContext() {
		return DefaultSaveContext.builder().suppressAutowatch(true).suppressNotifications(true).updateLastModifier(true).updateTrigger(PageUpdateTrigger.EDIT_PAGE).build();
	}
}
