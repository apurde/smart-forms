package de.edrup.confluence.plugins.smartforms.search;

import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;

import javax.inject.Inject;

import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.plugins.index.api.Extractor2;
import com.atlassian.confluence.plugins.index.api.FieldDescriptor;
import com.atlassian.confluence.plugins.index.api.FieldDescriptor.Store;
import com.atlassian.confluence.plugins.index.api.LongFieldDescriptor;
import com.atlassian.confluence.plugins.index.api.StringFieldDescriptor;
import com.atlassian.confluence.plugins.index.api.TextFieldDescriptor;
import com.atlassian.json.jsonorg.JSONObject;
import com.google.common.collect.ImmutableList;

import de.edrup.confluence.plugins.smartforms.util.SmartFormsHelper;

public class SmartFormsDataExtractor2 implements Extractor2 {
	
	private final SmartFormsHelper smartFormsHelper;
	
	
	@Inject
	public SmartFormsDataExtractor2(SmartFormsHelper smartFormsHelper) {
		this.smartFormsHelper = smartFormsHelper;
	}
	
	
	@Override
	public StringBuilder extractText(Object searchable) {
		
		StringBuilder extractedText = new StringBuilder();
		
		if(searchable instanceof Page) {
			if(((Page) searchable).getBodyAsString().contains("smart-forms-instance")) {
		
				Long pageId = ((Page) searchable).getId();
				JSONObject data = smartFormsHelper.getAllFormData(pageId);
				Iterator<String> keys = data.keys();
				
				while(keys.hasNext()) {
					String key = keys.next();
					String value = data.getString(key);
					if(value.contains("multiple:") || value.contains("select2:")) {
						String[] splitValues = value.replace("multiple:", "").replace("select2:", "").split(",");
						extractedText.append(String.join(" ", splitValues).toString().concat(" "));
					}
					else {
						extractedText.append(value);
					}
				}
			}
		}
				
		return extractedText;
	}
	
	
	@Override
	public Collection<FieldDescriptor> extractFields(Object searchable) {
		
		ImmutableList.Builder<FieldDescriptor> builder = ImmutableList.builder();
		 
		if(searchable instanceof Page) {
			if(((Page) searchable).getBodyAsString().contains("smart-forms-instance")) {
				Long pageId = ((Page) searchable).getId();
				JSONObject data = smartFormsHelper.getAllFormData(pageId);
				Iterator<String> keys = data.keys();
				while(keys.hasNext()) {
					String key = keys.next();
					String value = data.getString(key);
					if(value.contains("multiple:") || value.contains("select2:")) {
						String[] splitValues = value.replace("multiple:", "").replace("select2:", "").split(",");
						for(String splitValue : splitValues) {
							builder.add(new StringFieldDescriptor("smartformsdata", key + ":" + splitValue, Store.NO));
							builder.add(new TextFieldDescriptor("smartformsdatatx", cleanForAnalyzer(key + splitValue), Store.NO));
						}
					}
					else {
						builder.add(new StringFieldDescriptor("smartformsdata", key + ":" + value, Store.NO));
						builder.add(new TextFieldDescriptor("smartformsdatatx", cleanForAnalyzer(key + value), Store.NO));
					}
				}
				if(!smartFormsHelper.getDueDate(pageId).isEmpty()) {
					SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmmssSSS");
					builder.add(new StringFieldDescriptor("smartformsduedate", sdf.format(smartFormsHelper.convertString2Date(smartFormsHelper.getDueDate(pageId))), Store.NO));
				}
				HashMap<String, String> sortValues = smartFormsHelper.getSortValues(pageId);
				for(String sortName : sortValues.keySet()) {
					if(sortValues.get(sortName) != null) {
						builder.add(new StringFieldDescriptor(sortName, sortValues.get(sortName), Store.NO));
					}
				}
			}
		}
		
		return builder.build();
	}
	
	
	private String cleanForAnalyzer(String s) {
		return s.replaceAll("[^a-zA-Z0-9 ]", "");
	}
}
