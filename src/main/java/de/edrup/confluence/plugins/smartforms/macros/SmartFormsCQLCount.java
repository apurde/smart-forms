package de.edrup.confluence.plugins.smartforms.macros;

import java.util.Map;

import javax.inject.Inject;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;

import de.edrup.confluence.plugins.smartforms.util.SmartFormsHelper;


public class SmartFormsCQLCount implements Macro {
	
	private final SmartFormsHelper smartFormsHelper;
	
	@Inject
	public SmartFormsCQLCount(SmartFormsHelper smartFormsHelper) {
		this.smartFormsHelper = smartFormsHelper;
	}

	
	@Override
	public String execute(Map<String, String> parameters, String bodyContent, ConversionContext conversionContext) throws MacroExecutionException {
		int counter = smartFormsHelper.getPagesMatchingQuery(parameters.get("counter"), 0, 1).getTotalSize();
		
		if(parameters.containsKey("denominator")) {
			int denominator =  smartFormsHelper.getPagesMatchingQuery(parameters.get("denominator"), 0, 1).getTotalSize();
			if(denominator > 0) {
				return Double.toString(counter / denominator);
			}
			else {
				return "0.0";
			}
		}
		else {
			return Integer.toString(counter); 
		}
	}
	

	@Override
	public BodyType getBodyType() {
		return BodyType.NONE;
	}

	
	@Override
	public OutputType getOutputType() {
		return OutputType.BLOCK;
	}
}
