package de.edrup.confluence.plugins.smartforms.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.atlassian.json.jsonorg.JSONObject;

public class SafeJSONObject extends JSONObject {
	
	private static final Logger log = LoggerFactory.getLogger(SafeJSONObject.class);
	
	private enum TYPE {
		INT, STRING, BOOLEAN, ARRAY
	}
	
	
	public SafeJSONObject(String fromString) {
		super(fromString);
	}
	
	
	public SafeJSONObject() {
		super();
	}
	
	
	public String safeGetString(String key, String defaultValue) {
		try {
			if(has(key)) {
				return getString(key);
			}
		}
		catch(Exception e) {
			log.error(e.toString());
		}
		
		return defaultValue;
	}
	
	
	public boolean safeGetBoolean(String key, boolean defaultValue) {
		try {
			if(has(key)) {
				return getBoolean(key);
			}
		}
		catch(Exception e) {
			log.error(e.toString());
		}
		
		return defaultValue;
	}
	
	
	public int safeGetInt(String key, int defaultValue) {
		try {
			if(has(key)) {
				return getInt(key);
			}
		}
		catch(Exception e) {
			log.error(e.toString());
		}
		
		return defaultValue;
	}

	
	public SafeJSONArray safeGetJSONArray(String key) {
		try {
			if(has(key)) {
				return new SafeJSONArray(getJSONArray(key).toString());
			}
		}
		catch(Exception e) {
			log.error(e.toString());
		}
		
		return new SafeJSONArray();
	}
	
	
	public boolean checkOptionalForTypeInt(String key) {
		return checkOptionalForType(key, TYPE.INT);
	}
	
	
	public boolean checkOptionalForTypeBoolean(String key) {
		return checkOptionalForType(key, TYPE.BOOLEAN);
	}
	
	
	public boolean checkOptionalForTypeString(String key) {
		return checkOptionalForType(key, TYPE.STRING);
	}
	
	
	private boolean checkOptionalForType(String key, TYPE type) {
		if(has(key)) {
			try {
				switch(type) {
					case BOOLEAN: getBoolean(key); break;
					case INT: getInt(key); break;
					case STRING: getString(key); break;
					default: break;
				}
				return true;
			}
			catch(Exception e) {
				return false;
			}
		}
		return true;
	}
}
