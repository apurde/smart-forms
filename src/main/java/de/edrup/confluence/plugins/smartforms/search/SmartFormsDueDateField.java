package de.edrup.confluence.plugins.smartforms.search;

import com.atlassian.confluence.plugins.cql.spi.fields.AbstractDateRangeFieldHandler;
import com.atlassian.confluence.search.v2.SearchSort;
import com.atlassian.confluence.search.v2.SearchSort.Order;
import com.atlassian.confluence.search.v2.query.DateRangeQuery;
import com.atlassian.confluence.search.v2.query.DateRangeQuery.Builder;

public class SmartFormsDueDateField extends AbstractDateRangeFieldHandler {
	
	private static final String FIELD_NAME = "smartformsduedate";

    public SmartFormsDueDateField() {
        super(FIELD_NAME);
    }

	@Override
	protected SearchSort getSearchSort(Order order) {
		return new SmartFormsDateSort(FIELD_NAME, order);
	}

	@Override
	protected Builder newDateRangeBuilder() {
		return DateRangeQuery.newDateRangeQuery(FIELD_NAME);
	}
}
