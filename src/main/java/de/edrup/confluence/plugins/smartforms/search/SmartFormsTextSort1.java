package de.edrup.confluence.plugins.smartforms.search;

public class SmartFormsTextSort1 extends SmartFormsEqualityFieldHandler {
	
	private static final String FIELD_NAME = "smartformstextsort1";
	
	public SmartFormsTextSort1() {
		super(FIELD_NAME);
	}
}
