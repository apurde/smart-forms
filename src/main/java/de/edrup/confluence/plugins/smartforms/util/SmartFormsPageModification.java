package de.edrup.confluence.plugins.smartforms.util;

import com.atlassian.confluence.core.Modification;
import com.atlassian.confluence.pages.Page;

public class SmartFormsPageModification implements Modification<Page> {
	
	private final String newBody;
	private final String versionComment;
	
	public SmartFormsPageModification(String newBody, String versionComment) {
		this.newBody = newBody;
		this.versionComment = versionComment;
	}

	@Override
	public void modify(Page p) {
		p.setBodyAsString(newBody);
		p.setVersionComment(versionComment);
	}
}
