package de.edrup.confluence.plugins.smartforms.search;

public class SmartFormsDataField extends SmartFormsEqualityFieldHandler {

	private static final String FIELD_NAME = "smartformsdata";

    public SmartFormsDataField() {
        super(FIELD_NAME);
    }
}
