package de.edrup.confluence.plugins.smartforms.macros;

import java.util.Map;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;


public class SmartFormsRenderedSection implements Macro {

	@Override
	public String execute(Map<String, String> parameeters, String bodyContent, ConversionContext comversionContext) throws MacroExecutionException {
		return bodyContent;
	}
	
	
	@Override
	public BodyType getBodyType() {
		return BodyType.RICH_TEXT;
	}

	
	@Override
	public OutputType getOutputType() {
		return OutputType.BLOCK;
	}
}
