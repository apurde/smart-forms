function SmartFormsOverview() {
	
	var registeredCallbacks = [];
	
	this.init = function() {
		AJS.$(".smart-forms-overview-container").each(function() {
			var section = this;
			
			AJS.$(section).find(".load-spinner").hide();
			
			AJS.$(section).find(".smart-forms-query-section:first").html(AJS.$(section).find(".smart-forms-query-controls:first").html());
			getOverview(section, parseInt(AJS.$(section).attr("page")));
			AJS.$(section).find(".smart-forms-query,.smart-forms-order-by").change(function() {
				getOverview(section, "1");
			});
			AJS.$(section).find(".smart-forms-text-query").on('keyup', function (e) {
			    if (e.keyCode === 13) {
			    	getOverview(section, "1");
			    }
			});
			AJS.$(section).find(".smart-forms-export-button").click(function() {
				exportToExcel(section);
			});
		});
	};
	
	
	// allow outside JS functions to trigger an overview reload
	this.reload = function(section) {
		getOverview(section, 1);
	};
	

	// get the overview data
	var getOverview = function(section, page) {
		
		var start = (page - 1) * parseInt(AJS.$(section).attr("limit"), 10);
		
		// build query
		var query = AJS.$(section).attr("query");
		query = query.replace("$currentPageId", AJS.Meta.get("page-id"));
		AJS.$(section).find(".smart-forms-query-section:first").find(".smart-forms-query").each(function() {
			if(AJS.$(this).val().length) {
				query = AJS.$(this).val() + " and " + query;
			}
		});
		AJS.$(section).find(".smart-forms-query-section:first").find(".smart-forms-order-by:first").each(function() {
			if(AJS.$(this).val().length && query.indexOf("order by") === -1) {
				query = query + " order by " +  AJS.$(this).val();
			}
		});
		AJS.$(section).find(".smart-forms-query-section:first").find(".smart-forms-text-query:first").each(function() {
			if(AJS.$(this).val().length) {
				query = "siteSearch ~ \"" + AJS.$(this).val() + "\" and " + query;
			}
		});
		
		// show spinner
		AJS.$(section).find(".load-spinner").show();
		
		// get overview
		AJS.$.ajax({
			url: AJS.contextPath() + "/rest/smartforms/1.0/overview",
			type: "GET",
			contentType: "text/html",
			data: {
				templatePageId : AJS.$(section).attr("overview-template-page-id"),
				macroId : AJS.$(section).attr("macro-id"),
				query : query,
				start : start,
				limit : AJS.$(section).attr("limit")
			},
			success: function(result) {
				AJS.$(section).attr("page", page);
				AJS.$(section).find(".load-spinner").hide();
				AJS.$(section).find(".smart-forms-table-section").html(result.renderedTemplate);
				convertToFriendlyDate(section);
				showPaginationControls(section, result.totalSize);
				prepareDragCandidates(section);
				prepareDropAreas(section);
				if(AJS.$(section).find("table").hasClass("aui-table-sortable")) {
					AJS.tablessortable.setTableSortable(AJS.$(section).find("table"));
				}
			},
			error: function(request, status, error) {
				AJS.$(section).find(".load-spinner").hide();
				AJS.$(section).find(".smart-forms-table-section").html("");
				showPaginationControls(section, 0);
			}
		});
	};
	
	
	// convert all dates to a friendly format
	var convertToFriendlyDate = function(section) {
		var elements = AJS.$(section).find(".smart-forms-friendly-date");
		for(var n = 0; n < elements.length; n++) {
			var date = new Date(AJS.$(elements[n]).attr("data-sort-value"));
			var now = new Date();
			AJS.$(elements[n]).text(formatDate(date));
		}
	};
	
	
	// replacement of friendlyFormatDateTime
	var formatDate = function(date) {
	    const now = new Date();
	    const diffInSeconds = (now - date) / 1000;
	    const absDiffInSeconds = Math.abs(diffInSeconds);
	
	    // Calculate time differences
	    const minutes = Math.floor(absDiffInSeconds / 60);
	    const hours = Math.floor(minutes / 60);
	    const days = Math.floor(hours / 24);
	
	    // Relative Time Format for less than x day(s)
	    if (days < 1) {
	        const rtf = new Intl.RelativeTimeFormat(navigator.language, { numeric: 'auto' });
	        
	        if (days > 0) {
	            return rtf.format(-days, 'day');
	        } else if (hours > 0) {
	            return rtf.format(-hours, 'hour');
	        } else {
	            return rtf.format(-minutes, 'minute');
	        }
	    } 
	
	    // Non-relative format for 3 or more days old or future dates
	    return date.toLocaleDateString(navigator.language, {
	        year: 'numeric',
	        month: 'short',
	        day: 'numeric'
	    });
	};
	
	
	// show pagination controls
	var showPaginationControls = function(section, totalSize) {
		
		var currentPage = parseInt(AJS.$(section).attr("page"));
		var pageSize = parseInt(AJS.$(section).attr("limit"));
		var numPages = Math.ceil(totalSize / pageSize);
		
		// don't show any links in case there are no results
		if(numPages <= 1) {
			AJS.$(section).find(".smart-forms-pagination-section").html("");
			return;
		}
		
		// previous control
		var navLink = "<span class='smart-forms-pagination-span' style='margin-left:0px' page='prevPageNum'><a class='smart-forms-pagination-link smart-forms-pagination-link-prev' page='prevPageNum' href='#'>" + AJS.I18n.getText("de.edrup.confluence.plugins.smart-forms.overview.previous") + "</a></span>";
		navLink = navLink.replace(/prevPageNum/g, currentPage - 1);
		var navLinks = navLink;

		// first page
		navLinks += "<span class='smart-forms-pagination-span' page='1'><a class='smart-forms-pagination-link' href='#' page='1'>1</a></span>";

		// surrounding pages
		var start = currentPage > 5 ? currentPage - 4 : 2;
		var end = currentPage < numPages - 4 ? currentPage + 4 : numPages - 1;
		
		// dots needed?
		if(start > 2) {
			navLinks += "<span class='smart-forms-pagination-dots'>...</span>";
		}

		// the links to the surrounding pages
		for(var n = start; n <= end; n++) {
			var navLink = "<span class='smart-forms-pagination-span' page='pageNum'><a class='smart-forms-pagination-link' href='#' page='pageNum'>pageNum</a></span>";
			navLink = navLink.replace(/pageNum/g, n);
			navLinks = navLinks + navLink;
		}

		// dots needed?
		if(end < numPages - 1) {
			navLinks += "<span class='smart-forms-pagination-dots'>...</span>";
		}

		// last page
		if(numPages > 1) {
			var navLink = "<span class='smart-forms-pagination-span' page='pageNum'><a class='smart-forms-pagination-link' href='#' page='pageNum'>pageNum</a></span>";
			navLink = navLink.replace(/pageNum/g, numPages);
			navLinks = navLinks + navLink;
		}

		// next control
		var navLink = "<span class='smart-forms-pagination-span' page='nextPageNum'><a class='smart-forms-pagination-link smart-forms-pagination-link-next' href='#' page='nextPageNum'>" + AJS.I18n.getText("de.edrup.confluence.plugins.smart-forms.overview.next") + "</a></span>";
		navLink = navLink.replace(/nextPageNum/g, currentPage + 1);
		navLinks = navLinks + navLink;

		// set controls
		AJS.$(section).find(".smart-forms-pagination-section").html(navLinks);
				
		// disable the link to the current page and enable all other ones
		AJS.$(section).find(".smart-forms-pagination-link").removeClass("smart-forms-pagination-link-inactive");
		AJS.$(section).find(".smart-forms-pagination-link").removeClass("smart-forms-pagination-link-current");
		AJS.$(section).find("a[page='" + currentPage + "']").addClass("smart-forms-pagination-link-current");
		
		// highlight the current span
		AJS.$(section).find(".smart-forms-pagination-span").removeClass("smart-forms-pagination-span-current");
		AJS.$(section).find("span[page='" + currentPage + "']").addClass("smart-forms-pagination-span-current");
		
		// disable prev and next link depending on the page we are on
		if(currentPage == 1) {
			AJS.$(section).find(".smart-forms-pagination-link-prev").addClass("smart-forms-pagination-link-inactive");
		}
		if(currentPage == numPages) {
			AJS.$(section).find(".smart-forms-pagination-link-next").addClass("smart-forms-pagination-link-inactive");
		}
		
		// bind the pagination controls
		AJS.$(section).find(".smart-forms-pagination-link").each(function () {
			AJS.$(this).click(function() {
				getOverview(AJS.$(this).closest(".smart-forms-overview-container"), parseInt(AJS.$(this).attr("page")));
			});
		});
	};
	
	
	// prepare all instances which can be dragged
	var prepareDragCandidates = function(section) {
		AJS.$(section).find(".smart-forms-drag").each(function() {
			AJS.$(this).attr("draggable", "true");
			AJS.$(this).on("dragstart", function(e) {
				e.originalEvent.dataTransfer.setData("text", AJS.$(e.originalEvent.target).data("page-id"));
			});
		});
	};
	
	
	// prepare all drop zones
	var prepareDropAreas = function(section) {
		AJS.$(section).find(".smart-forms-drop").each(function() {
			AJS.$(this).on("drop", function(e) {
				tryTransitionTo(e.originalEvent.dataTransfer.getData("text"), AJS.$(this).data("toState"), this);
			});
		});
	};
	
	
	// try the transition of the given page ID to the defined state
	var tryTransitionTo = function(pageID, toState, dropOnElement) {
		var data = {
			pageId : pageID,
			to : toState	
		};
		AJS.$.ajax({
			url: AJS.contextPath() + "/rest/smartforms/1.0/findTransitionAndExecute?" + AJS.$.param(data),
			type: "PUT",
			dataType: "json",
			success: function(result) {
				if(result.success) {
					AJS.$(dropOnElement).prepend(AJS.$('.smart-forms-drag[data-page-id="' + pageID +  '"]:first'));
				}
				else {
					alert(result.details);
				}
			}
		});
	};
	
	
	// export to Excel
	var exportToExcel = function(section) {
		AJS.$(section).find(".load-spinner").show();
		AJS.$(section).find(".smart-forms-export-button").hide();
		var query = AJS.$(section).attr("query");
		var columns = AJS.$(section).attr("columns");
		query = query.replace("$currentPageId", AJS.Meta.get("page-id"));
		AJS.$.ajax({
			url: AJS.contextPath() + "/rest/smartforms/1.0/export",
			type: "GET",
			data: {
				query : query,
				columns : columns,
			},
			success: function(result) {
				AJS.$(section).find(".load-spinner").hide();
				AJS.$(section).find(".smart-forms-export-button").show();
				var a = document.createElement('a');
				a.href = result;
				a.download = result.split('/').pop();
				document.body.appendChild(a);
				a.click();
				document.body.removeChild(a);
			},
			error: function() {
				AJS.$(section).find(".load-spinner").hide();
				AJS.$(section).find(".smart-forms-export-button").show();
			}
		});
	};
}

var smartFormsOverview = new SmartFormsOverview();

AJS.toInit(function() {
	smartFormsOverview.init();
});