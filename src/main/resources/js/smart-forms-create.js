function SmartFormsCreate() {
	
	this.init = function() {
		AJS.$("#content").on("click", ".smart-forms-create-link", function() {
			doCreate(this, AJS.$(this).data("form-page-id"), AJS.$(this).data("workflow-page-id"), AJS.$(this).data("parent-page-id"), AJS.$(this).data("key"));
			return false;
		});
	};
	
	
	// assign the instance to the given user
	var doCreate = function(element, formPageId, workflowPageId, parentPageId, key) {
		
		if(parentPageId.length == 0) {
			parentPageId = AJS.Meta.get("page-id");
		} 
		
		AJS.$(element).closest(".smart-forms-create-div").find(".smart-forms-create-link").hide();
		AJS.$(element).closest(".smart-forms-create-div").find(".smart-forms-create-spinner").show();
		
		var data = {
			formPageId : formPageId,
			workflowPageId : workflowPageId,
			parentPageId : parentPageId,
			key : key
		};
		
		AJS.$.ajax({
			url: AJS.contextPath() + "/rest/smartforms/1.0/createNewFormPage?" + AJS.$.param(data),
			type: "PUT",
			dataType: "json",
			success: function(result) {
				window.location.href =  AJS.contextPath() + "/pages/viewpage.action?initialLoad=true&pageId=" + result.id
			},
			error: function() {
				AJS.flag({
					type: "error",
					body: AJS.I18n.getText("de.edrup.confluence.plugins.smart-forms.create.error")
				});
			}
		});
	};
}

var smartFormsCreate = new SmartFormsCreate();

AJS.toInit(function() {
	smartFormsCreate.init();
});