function SmartFormsInstance() {
	
	var dirty = false;
	var version = 0;
	
	this.init = function() {
		
		// extend jQuery
		// https://stackoverflow.com/questions/5060652/jquery-selector-for-input-value
		AJS.$.extend(
		  AJS.$.expr[':'],
		  {
		    /// check that a field's value property has a particular value
		    'field-value': function (el, indx, args) {
		      var a, v = $(el).val() || "";
		      if ( (a = args[3]) ) {
		        switch ( a.charAt(0) ) {
		          /// begins with
		          case '^':
		            return v.substring(0,a.length-1) == a.substring(1,a.length);
		          break;
		          /// ends with
		          case '$':
		            return v.substr(v.length-a.length-1,v.length) == 
		              a.substring(1,a.length);
		          break;
		          /// contains
		          case '*': return v.indexOf(a.substring(1,a.length)) != -1; break;
		          /// equals
		          case '=': return v == a.substring(1,a.length); break;
		          /// not equals
		          case '!': return v != a.substring(1,a.length); break;
		          /// equals
		          default: return v == a; break;
		        }
		      }
		      else {
		        return !!v;
		      }
		    }
		  }
		);
				
		var currentUsersGroups = AJS.$("#smart-forms-form-section").attr("current-users-groups") || ""; // Ensure it's not null/undefined
		
		// Hide fields which should be hidden (legacy)
		AJS.$("[limit-visibility-to-group]").each(function() {
		    var group = AJS.$(this).attr("limit-visibility-to-group");
		    if (currentUsersGroups.indexOf(group) === -1) {
		        AJS.$(this).hide();
		    }
		});
		
		// Hide fields which should be hidden (new)
		var currentUsersGroupsArray = currentUsersGroups.split(";");
		AJS.$("[limit-visibility-to-groups]").each(function() {
		    var visibilityGroups = (AJS.$(this).attr("limit-visibility-to-groups") || "").split(/\s*,\s*/); // Ensure it's not null/undefined
		    if (!visibilityGroups.some(group => currentUsersGroupsArray.includes(group))) {
		        AJS.$(this).hide();
		    }
		});

		// populate form
		if(AJS.$("#smart-forms-form-section").length) {
			populate(AJS.$("#smart-forms-form-section").find("form:first"), JSON.parse(AJS.$("#smart-forms-form-section").attr("current-form-data")));
			version = AJS.$("#smart-forms-form-section").attr("current-form-version");
		}
		
		// hide edit button
		AJS.$("#editPageLink").hide();
		
		// hide table
		AJS.$(".smart-forms-table").hide();
		
		// init user selection for assignment
		if(AJS.$("#smart-forms-assign-section").length) {
			initUserSelection();
		}
		
		// init user selectors
		AJS.$(".smart-forms-user-selector").each(function() {
			var currentValue = AJS.$(this).val();
			var template = AJS.$(this).attr("template") || "$fullName ($name)";
			initUserSelector(this, AJS.I18n.getText("de.edrup.confluence.plugins.smart-forms.form.userSelect.placeholder"),
				AJS.$(this).attr("user-group"), AJS.$(this).data("multiple") === true, template);
			if(AJS.$(this).data("multiple") === true) {
				AJS.$(this).auiSelect2('data', splitToData(currentValue.replace("select2:", ""), ","));
			}
			else {
				AJS.$(this).auiSelect2('data', splitToData(currentValue.replace("select2:", ""), ",")[0]);	
			}
			AJS.$(this).val(currentValue.replace("select2:", ""));
		});
		
		// init other select2s
		AJS.$(".smart-forms-select2").each(function() {
			AJS.$(this).auiSelect2();
		});
		
		// catch date pickers
		AJS.$(".smart-forms-date-picker").each(function() {
			AJS.$(this).datePicker({'overrideBrowserDefault': true});
		});
		
		// check whether this is an initial load
		if(window.location.href.indexOf("initialLoad") > -1) {
			if(window.location.href.indexOf("cloned") === -1) {
				saveFormData();
			}
			enableForm();
			history.pushState(null, null, window.location.href.replace("initialLoad=true&", ""));
		}
		else {
			disableForm();
		}
		
		// avoid exit in edit mode without confirmation
		AJS.$(window).on("beforeunload", function() {
			if(dirty) {
				return "Are you sure that you want to leave without saving?";
			}
		});	
		
		// disable uploads when there is no save section
		if(AJS.$("#smart-forms-save-section").length === 0) {
			setTimeout(function() {
				AJS.$("#page").on("drop", function(event) { event.preventDefault(); return false; });
				AJS.$("#view-attachments-link").remove();
				AJS.$(".attachment-boxes").unbind("drop");
				AJS.$("input[name='file_0']").disable();
			}, 200);
		}	
		
		// bind clone form
		AJS.$("#smart-forms-clone-button").click(function() {
			cloneForm();
		});
		
		checkQueryConditions();
	};
	
	
	this.edit = function() {
		enableForm();
	};
	
	
	this.save = function() {
		if(AJS.$("#smart-forms-form-section").find("form:first [data-aui-validation-state='invalid']").length === 0) {
			saveFormData();
			disableForm();
			checkQueryConditions();
		}
	};
	
	
	this.cancel = function() {
		disableForm();
		location.reload();
	};
	
	
	this.executeTransition = function(transitionName) {
		executeTransitionI(transitionName);
	};
	
	
	this.assign = function(doAssign) {
		document.getElementById('smart-forms-user-selector').open = false;
		if(doAssign) {
			assignTo(AJS.$("#smart-forms-user").auiSelect2("data").name);
		}
		AJS.$("#smart-forms-user").val("");
		AJS.$("#smart-forms-user").auiSelect2("data", null);

	};
	
	
	this.assignToMe = function() {
		assignTo(AJS.Meta.get("remote-user"));
	};
	
	
	this.unassign = function() {
		assignTo("");
	};
	
	
	// populate the form data
	var populate = function(frm, data) { 
	    AJS.$.each(data, function(key, value) {  
			key = key.replace(" ", "");
	        var ctrl = AJS.$('[name='+key+']', frm);  
	        switch(ctrl.prop("type")) { 
	            case "radio": case "checkbox":   
	                ctrl.each(function() {
	                    if(AJS.$(this).attr('value') == value) AJS.$(this).attr("checked",value);
	                });   
	                break; 
	            default:
	            	if(ctrl.attr("multiple") == "multiple") {
	            		AJS.$.each(value.replace("multiple:", "").split(","), function(i, e) {
	            			AJS.$(ctrl).find("option[value='" + e + "']").prop("selected", true);
	            		});
	            	}
	            	else {
	            		 ctrl.val(value); 
	            	}
	        }
	    });  
	};
	
	
	// save form data
	var saveFormData = function() {
		
		var form = AJS.$("#smart-forms-form-section").find("form:first");
		var formData = getFormData(form);
		
		AJS.$.ajax({
			url: AJS.contextPath() + "/rest/smartforms/1.0/saveFormData?pageId=" + AJS.Meta.get("page-id") + "&version=" + version,
			type: "PUT",
			contentType: "text/plain",
			data: formData,
			success: function(result) {
				version = result.version;
			},
			error: function() {
				AJS.flag({
				    type: 'error',
				    body: AJS.I18n.getText("Error saving the form data - please reload the page and try again.")
				});
			}
		});
	};
	
	
	// enable entering into the form
	var enableForm = function() {
		
		dirty = true;
		
		AJS.$(".smart-forms-form input").prop("disabled", false);
		AJS.$(".smart-forms-form textarea").prop("disabled", false);
		AJS.$(".smart-forms-form select").prop("disabled", false);
		AJS.$(".smart-forms-user-selector").each(function() {
			AJS.$(this).auiSelect2("enable");
		});
		
		var currentUsersGroups = AJS.$("#smart-forms-form-section").attr("current-users-groups");
		AJS.$("[limit-access-to-group]").each(function() {
			if(currentUsersGroups.indexOf($(this).attr("limit-access-to-group")) == -1) {
				AJS.$(this).prop("disabled", true);
			}
		});
		
		AJS.$("#smart-forms-save-button").show();
		AJS.$("#smart-forms-cancel-button").show();
		AJS.$("#smart-forms-edit-button").hide();
		AJS.$("#smart-forms-clone-button").hide();
		AJS.$(".smart-forms-transition-button").attr("aria-disabled", "true");
		AJS.$(".smart-forms-transition-button").prop("disabled", true);
		AJS.$(".plugin_attachments_container,.attachment-boxes,.smart-forms-hide-during-edit,.smart-forms-overview-container").hide();
		AJS.$("#smart-forms-upload-hint,.smart-forms-show-during-edit").show();
	};
	
	
	// disable entering into the form
	var disableForm = function() {
		
		dirty = false;
		
		AJS.$(".smart-forms-form input").prop("disabled", true);
		AJS.$(".smart-forms-form select").prop("disabled", true);
		AJS.$(".smart-forms-form textarea").prop("disabled", true);
		AJS.$(".smart-forms-user-selector").each(function() {
			AJS.$(this).auiSelect2("disable");
		});
		AJS.$("#smart-forms-save-button").hide();
		AJS.$("#smart-forms-cancel-button").hide();		
		AJS.$("#smart-forms-edit-button").show();
		AJS.$("#smart-forms-clone-button").show();
		AJS.$(".smart-forms-transition-button").attr("aria-disabled", "false");
		AJS.$(".smart-forms-transition-button").prop("disabled", false);
		AJS.$(".plugin_attachments_container,.attachment-boxes,.smart-forms-hide-during-edit,.smart-forms-overview-container").show();
		AJS.$("#smart-forms-upload-hint,.smart-forms-show-during-edit").hide();
	};
	
	
	// get the form data
	var getFormData = function(form) {
		
		AJS.$("[limit-access-to-group]").each(function() {
			AJS.$(this).prop("disabled", false);
		});
		
	    var unindexed_array = AJS.$(form).serializeArray();
	    var indexed_array = {};

	    AJS.$.map(unindexed_array, function(n, i) {
	    	if(n['name'] in indexed_array) {
	    		if(indexed_array[n['name']].indexOf("multiple:") < 0) {
	    			indexed_array[n['name']] = "multiple:" + indexed_array[n['name']];
	    		}
	    		indexed_array[n['name']] += "," + n['value'];
	    	}
	    	else {
	    		indexed_array[n['name']] = n['value'];
	    	}
	    });
	    
	    AJS.$(".smart-forms-user-selector").each(function() {
	    	if($(this).attr("name")) {
	    		indexed_array[$(this).attr("name")] = "select2:" + indexed_array[$(this).attr("name")];
	    	}
	    });

	    console.log(indexed_array);
	    
	    return JSON.stringify(indexed_array);
	};
	
	
	// execute a transition
	var executeTransitionI = function(transitionName) {
		
		AJS.$(".smart-forms-transition-button").hide();
		AJS.$("#smart-forms-workflow-spinner").spin();
		
		var data = {
			pageId : AJS.Meta.get("page-id"),
			transitionName : transitionName
		};
		
		AJS.$.ajax({
			url: AJS.contextPath() + "/rest/smartforms/1.0/executeTransition?" + AJS.$.param(data),
			type: "PUT",
			dataType: "json",
			success: function(result) {
				if(result.success) {
					location.reload();
				}
				else {
					AJS.$(".smart-forms-transition-button").show();
					AJS.$("#smart-forms-workflow-spinner").spinStop();
					alert(result.details);
				}
			},
			error: function(request, status, error) {
				AJS.$("#smart-forms-workflow-spinner").spinStop();
			}
		});
	};
	
	
	// init the user selection
	var initUserSelection = function() {
		
		// init user selector
		initUserSelector(AJS.$("#smart-forms-user"),
			AJS.I18n.getText("de.edrup.confluence.plugins.smart-forms.form.userSelect.placeholder"),
			AJS.$("#smart-forms-assign-section").attr("assignment-group"), false, "$fullName ($name)");
		
		// fix select2 alignment
		AJS.$(".select2-container").css("vertical-align", "top");
		
		// remove mask to allow outside clicks
		AJS.$('#smart-forms-user-selector').on("select2-open", function() {
			AJS.$('.select2-drop-mask').remove();
		});
		
		// close mask when inline dialog is closed
		AJS.$("#smart-forms-user-selector").on("aui-hide", function() {
			AJS.$("#smart-forms-user").select2("close");
			return true;
		});
	};
	
	
	// init the the select2 selector
	var initUserSelector = function(element, placeholderText, groupName, multiple, template) {
		
		AJS.$(element).auiSelect2({
			
			// let the user enter three letters before we search
		    minimumInputLength: 2,
		    
		    // set the placeholder
		    placeholder: placeholderText,
		    multiple : multiple,
		    		    
		    // let select2 load matching users
		    ajax: {
		        url: AJS.contextPath() + '/rest/smartforms/1.0/getUsers',
		        dataType: 'json',
		        type: "GET",
		        data: function (term) {
		            return {
		                groupName: groupName,
		                query: term
		            };
		        },
		        // map the results to a select2 format
		        results: function (response) {
		            return {
		                results: $.map(response, function (item) {
							var entry = template.replace("$fullName", item.fullName).replace("$name", item.name).replace("$email", item.email);
		                    return {
		                        text: entry,
		                        id: entry,
								fullName: item.fullName,
								name: item.name,
								email: item.email
		                    };
		                })
		            };
		        },	
		    }
		});
	};
	

	// assign the form instance to the given user
	var assignTo = function(userName) {
		
		AJS.$("#smart-forms-workflow-spinner").spin();
		
		var data = {
			pageId : AJS.Meta.get("page-id"),
			userName : userName	
		};
		
		AJS.$.ajax({
			url: AJS.contextPath() + "/rest/smartforms/1.0/assignUser?" + AJS.$.param(data),
			type: "PUT",
			dataType: "json",
			success: function(result) {
				location.reload();
			},
			error: function(request, status, error) {
				AJS.$("#smart-forms-workflow-spinner").spinStop();
			}
		});
	};
	
	
	// create a data array for select2 from the provided separated string
	var splitToData = function(stringData, seperator) {
		var splits = stringData.split(new RegExp(seperator));
		var dataArray = [];
		for(var n = 0; n < splits.length; n++) {
			if(splits[n].length > 0) {
				var dataSet = {
					id: splits[n],
					text: splits[n]
				};
				dataArray.push(dataSet);
			}
		}
		return dataArray;
	};
	
	
	// clone the form
	var cloneForm = function() {
		AJS.$("#smart-forms-clone-spinner").show();
		AJS.$("#smart-forms-clone-button").hide();
		AJS.$.ajax({
			url: AJS.contextPath() + "/rest/smartforms/1.0/cloneFormPage?clonePageId=" + AJS.Meta.get("page-id"),
			type: "PUT",
			dataType: "json",
			success: function(result) {
				window.location.href =  AJS.contextPath() + "/pages/viewpage.action?initialLoad=true&cloned=true&pageId=" + result.id
			}
		});
	};
	
	
	// check query conditions for transitions
	var checkQueryConditions = function() {
		AJS.$(".smart-forms-transition-button").each(function() {
			var queryCondition = AJS.$(this).data("query-condition");
			if(queryCondition.length > 0) {
				AJS.$(this).toggle(AJS.$(queryCondition).length > 0);
			}
		});
	};
}

var smartFormsInstance = new SmartFormsInstance();

AJS.toInit(function() {
	smartFormsInstance.init();
});