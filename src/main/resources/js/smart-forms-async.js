function SmartFormsAsync() {
	
	this.init = function() {
		AJS.$(".smart-forms-async").each(function() {
			var section = this;
			getRenderedMacro(section, AJS.$(section).data("macro-id"));
		});
	};
	
	
	var getRenderedMacro = function(section, macroId) {
		AJS.$.ajax({
			url: AJS.contextPath() + "/rest/smartforms/1.0/async?pageId=" + AJS.Meta.get("page-id") + "&macroId=" + macroId,
			type: "GET",
			success: function(result) {
				AJS.$(section).html(result);
			}
		});
	};
}

var smartFormsAsync = new SmartFormsAsync();

AJS.toInit(function() {
	smartFormsAsync.init();
});